#!/usr/bin/env bash

git checkout master && \
    git pull && \
    git push && \
    git checkout production && \
    git merge master && \
    git pull && \
    git push && \
    git checkout master

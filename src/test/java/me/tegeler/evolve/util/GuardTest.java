package me.tegeler.evolve.util;

import me.tegeler.evolve.exceptions.WorkingDirectoryNotEnabledException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class GuardTest {

	@Test
	public void testWorkingDirectoryIsNotEnabled() {
		Guard guard = new Guard();
		try {
			guard.checkIfWorkingDirectoryIsEnabled("/tmp");
			fail("Expected WorkingDirectoryNotEnabledException to be thrown");
		} catch (WorkingDirectoryNotEnabledException e) {
			assertEquals("Can't find /tmp/evolve.json as the starting point for the working directory.", e.getMessage());
		}
	}

	@Test
	public void testWorkingDirectoryIsEnabled() throws IOException {
		File evolve = new File("/tmp/evolve.json");
		assertTrue(evolve.createNewFile());
		Guard guard = new Guard();
		try {
			guard.checkIfWorkingDirectoryIsEnabled("/tmp");
		} catch (WorkingDirectoryNotEnabledException e) {
			fail("WorkingDirectoryNotEnabledException shouldn't be thrown");
		}
		assertTrue(evolve.delete());
	}

}
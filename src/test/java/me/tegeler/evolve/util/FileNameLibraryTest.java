package me.tegeler.evolve.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class FileNameLibraryTest {

	@Test
	public void test() {
		assertEquals(".api.json", FileNameLibrary.getApiModelFileNameEnding());
		assertEquals(".entity.json", FileNameLibrary.getEntityModelFileNameEnding());
		assertEquals(".form.json", FileNameLibrary.getFormModelFileNameEnding());
		assertEquals(".redux.json", FileNameLibrary.getReduxModelFileNameEnding());
		assertEquals(".api.evolve.ts", FileNameLibrary.getApiGeneratedFileNameEnding());
		assertEquals(".entity.evolve.ts", FileNameLibrary.getEntityGeneratedFileNameEnding());
		assertEquals(".form.evolve.ts", FileNameLibrary.getFormGeneratedFileNameEnding());
		assertEquals(".redux.evolve.ts", FileNameLibrary.getReduxGeneratedFileNameEnding());
	}
}
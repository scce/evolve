package me.tegeler.evolve.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringHelperTest {

    @Test
    public void test() {
        assertEquals("Test", StringHelper.firstToUpperCase("test"));
        assertEquals("test", StringHelper.firstToLowerCase("Test"));
    }
}
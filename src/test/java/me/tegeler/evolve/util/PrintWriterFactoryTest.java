package me.tegeler.evolve.util;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.Assert.*;

public class PrintWriterFactoryTest {
    @Test
    public void test() throws FileNotFoundException {
        PrintWriterFactory printWriterFactory = new PrintWriterFactory();
        File file = new File("/tmp/printWriterFactoryTest");
        assertEquals(PrintWriter.class, printWriterFactory.buildPrintWriter(file).getClass());
    }
}
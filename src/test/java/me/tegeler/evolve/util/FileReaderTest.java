package me.tegeler.evolve.util;

import me.tegeler.evolve.AbstractResourceBasedTest;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;

public class FileReaderTest extends AbstractResourceBasedTest {

    private final FileReader fileReader = new FileReader();

    @Test
    public void test() throws IOException {
        //given
        Path path = getTestResourceFile("util/FileReaderTestFile.txt").toPath();

        //when
        String actual = new String(fileReader.read(path));

        //then
        assertEquals("test", actual);
    }

}
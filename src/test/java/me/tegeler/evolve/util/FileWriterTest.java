package me.tegeler.evolve.util;

import me.tegeler.evolve.commands.generate.Rendered;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.mockito.Mockito.*;

public class FileWriterTest {
    @Test
    public void test() throws FileNotFoundException {
        //given
        File file = new File("/tmp/fileWriterTestFile");
        String renderedString = "";
        Rendered rendered = new Rendered(file.toPath(), renderedString);

        PrintWriterFactory printWriterFactory = mock(PrintWriterFactory.class);
        PrintWriter printWriter = mock(PrintWriter.class);
        when(printWriterFactory.buildPrintWriter(file)).thenReturn(printWriter);

        //when
        FileWriter fileWriter = new FileWriter(printWriterFactory);
        fileWriter.write(file, rendered);

        //then
        verify(printWriterFactory, times(1)).buildPrintWriter(file);
        verify(printWriter, times(1)).print(renderedString);
        verify(printWriter, times(1)).flush();
        verify(printWriter, times(1)).close();
    }
}
package me.tegeler.evolve.util;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DirectoryResolverTest {

	@Test
	public void test() {
		DirectoryResolver directoryResolver = new DirectoryResolver();
		String workingDirectory = directoryResolver.resolveWorkingDirectory();
		assertTrue(workingDirectory.length() > 0);
	}
}
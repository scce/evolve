package me.tegeler.evolve.commands;

import me.tegeler.evolve.util.DirectoryResolver;
import me.tegeler.evolve.util.FileDeleter;
import me.tegeler.evolve.util.Generator;
import me.tegeler.evolve.util.Guard;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

public class GenerateTest {

    private Generate generate;
    private DirectoryResolver resolver;
    private FileDeleter fileDeleter;
    private Generator generator;
    private Guard guard;

    @Before
    public void setUp() {
        resolver = mock(DirectoryResolver.class);
        fileDeleter = mock(FileDeleter.class);
        generator = mock(Generator.class);
        guard = mock(Guard.class);
        generate = new Generate(resolver, fileDeleter, generator, guard);
    }

    @Test
    public void test() throws Exception {
        //given
        String directory = "directory";
        when(resolver.resolveWorkingDirectory()).thenReturn(directory);

        //when
        Void callResult = generate.call();

        //then
        assertNull(callResult);
        verify(guard, times(1)).checkIfWorkingDirectoryIsEnabled(directory);
        verify(fileDeleter, times(1)).deleteGeneratedApiFiles(directory);
        verify(fileDeleter, times(1)).deleteGeneratedEntityFiles(directory);
        verify(fileDeleter, times(1)).deleteGeneratedFormFiles(directory);
        verify(generator, times(1)).generateApiFiles(directory);
        verify(generator, times(1)).generateEntityFiles(directory);
        verify(generator, times(1)).generateFormFiles(directory);
    }

    @Test
    public void testApi() throws Exception {
        //given
        String directory = "directory";
        when(resolver.resolveWorkingDirectory()).thenReturn(directory);

        //when
        generate.setGeneratorTask(GeneratorTask.api);
        Void callResult = generate.call();

        //then
        assertNull(callResult);
        verify(guard, times(1)).checkIfWorkingDirectoryIsEnabled(directory);
        verify(fileDeleter, times(1)).deleteGeneratedApiFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedEntityFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedFormFiles(directory);
        verify(generator, times(1)).generateApiFiles(directory);
        verify(generator, times(0)).generateEntityFiles(directory);
        verify(generator, times(0)).generateFormFiles(directory);
    }

    @Test
    public void testEntity() throws Exception {
        //given
        String directory = "directory";
        when(resolver.resolveWorkingDirectory()).thenReturn(directory);

        //when
        generate.setGeneratorTask(GeneratorTask.entity);
        Void callResult = generate.call();

        //then
        assertNull(callResult);
        verify(guard, times(1)).checkIfWorkingDirectoryIsEnabled(directory);
        verify(fileDeleter, times(0)).deleteGeneratedApiFiles(directory);
        verify(fileDeleter, times(1)).deleteGeneratedEntityFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedFormFiles(directory);
        verify(generator, times(0)).generateApiFiles(directory);
        verify(generator, times(1)).generateEntityFiles(directory);
        verify(generator, times(0)).generateFormFiles(directory);
    }

    @Test
    public void testForm() throws Exception {
        //given
        String directory = "directory";
        when(resolver.resolveWorkingDirectory()).thenReturn(directory);

        //when
        generate.setGeneratorTask(GeneratorTask.form);
        Void callResult = generate.call();

        //then
        assertNull(callResult);
        verify(guard, times(1)).checkIfWorkingDirectoryIsEnabled(directory);
        verify(fileDeleter, times(0)).deleteGeneratedApiFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedEntityFiles(directory);
        verify(fileDeleter, times(1)).deleteGeneratedFormFiles(directory);
        verify(generator, times(0)).generateApiFiles(directory);
        verify(generator, times(0)).generateEntityFiles(directory);
        verify(generator, times(1)).generateFormFiles(directory);
    }

    @Test
    public void testRedux() throws Exception {
        //given
        String directory = "directory";
        when(resolver.resolveWorkingDirectory()).thenReturn(directory);

        //when
        generate.setGeneratorTask(GeneratorTask.redux);
        Void callResult = generate.call();

        //then
        assertNull(callResult);
        verify(guard, times(1)).checkIfWorkingDirectoryIsEnabled(directory);
        verify(fileDeleter, times(0)).deleteGeneratedApiFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedEntityFiles(directory);
        verify(fileDeleter, times(0)).deleteGeneratedFormFiles(directory);
        verify(fileDeleter, times(1)).deleteGeneratedReduxFiles(directory);
        verify(generator, times(0)).generateApiFiles(directory);
        verify(generator, times(0)).generateEntityFiles(directory);
        verify(generator, times(0)).generateFormFiles(directory);
        verify(generator, times(1)).generateReduxFiles(directory);
    }
}
package me.tegeler.evolve.commands.generate.entity;

import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.entity.EntityRenderer;
import me.tegeler.evolve.commands.generate.entity.model.*;
import me.tegeler.evolve.target.bouncer.json.DecoderGenerator;
import me.tegeler.evolve.target.typescript.ImportGenerator;
import me.tegeler.evolve.target.typescript.InterfaceGenerator;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class EntityRendererTest {

    private EntityRenderer entityRenderer;

    public EntityRendererTest() throws IOException {
        Freemarker freemarker = new Freemarker();
        entityRenderer = new EntityRenderer(
                new ImportGenerator(freemarker),
                new InterfaceGenerator(freemarker),
                new DecoderGenerator(freemarker)
        );
    }

    @Test
    public void render() {
        Map<String, Entity> map = new HashMap<>();
        map.put("newCredentials", new EntityObject(false, false, false, null, "Credentials"));
        map.put("oldCredentials", new EntityObject(false, false, false, null, "Credentials"));
        map.put("age", new EntityNumber(false, false, true, null));
        map.put("female", new EntityBoolean(false, false, false, null));
        map.put("groups", new EntityString(true, false, false, null));
        map.put("email", new EntityString(false, false, false, null));
        map.put("name", new EntityString(false, false, false, null));

        String expected = "import {\n" +
                "    Decoder,\n" +
                "    array,\n" +
                "    boolean,\n" +
                "    field,\n" +
                "    number,\n" +
                "    object,\n" +
                "    optional,\n" +
                "    string\n" +
                "} from 'json-bouncer'\n" +
                "\n" +
                "import {\n" +
                "    Credentials,\n" +
                "    credentialsDecoder\n" +
                "} from './Credentials.entity.evolve'\n" +
                "\n" +
                "export interface User {\n" +
                "    readonly age: number | undefined\n" +
                "    readonly email: string\n" +
                "    readonly female: boolean\n" +
                "    readonly groups: string[]\n" +
                "    readonly name: string\n" +
                "    readonly newCredentials: Credentials\n" +
                "    readonly oldCredentials: Credentials\n" +
                "}\n" +
                "\n" +
                "export function userDecoder(): Decoder<User> {\n" +
                "    return object({\n" +
                "        age: field('age', optional(number())),\n" +
                "        email: field('email', string()),\n" +
                "        female: field('female', boolean()),\n" +
                "        groups: field('groups', array(string())),\n" +
                "        name: field('name', string()),\n" +
                "        newCredentials: field('newCredentials', credentialsDecoder()),\n" +
                "        oldCredentials: field('oldCredentials', credentialsDecoder()),\n" +
                "    })\n" +
                "}";
        Rendered rendered = entityRenderer.render(new Deserialized<>(Paths.get("/tmp/user.entity.json"), map));
        assertEquals(expected, rendered.getRendered());
    }

    @Test
    public void renderTag() {
        Map<String, Entity> map = new HashMap<>();
        map.put("tagAssignments", new EntityObject(true, false, false, "tag_assignments", "TagAssignment"));
        map.put("name", new EntityString(false, false, false, null));

        String expected = "import {\n" +
                "    Decoder,\n" +
                "    array,\n" +
                "    field,\n" +
                "    object,\n" +
                "    string\n" +
                "} from 'json-bouncer'\n" +
                "\n" +
                "import {\n" +
                "    TagAssignment,\n" +
                "    tagAssignmentDecoder\n" +
                "} from './TagAssignment.entity.evolve'\n" +
                "\n" +
                "export interface Tag {\n" +
                "    readonly name: string\n" +
                "    readonly tagAssignments: TagAssignment[]\n" +
                "}\n" +
                "\n" +
                "export function tagDecoder(): Decoder<Tag> {\n" +
                "    return object({\n" +
                "        name: field('name', string()),\n" +
                "        tagAssignments: field('tag_assignments', array(tagAssignmentDecoder())),\n" +
                "    })\n" +
                "}";
        Rendered rendered = entityRenderer.render(new Deserialized<>(Paths.get("/tmp/tag.entity.json"), map));
        assertEquals(expected, rendered.getRendered());
    }

    @Test
    public void renderTagAsset() {
        Map<String, Entity> map = new HashMap<>();
        map.put("id", new EntityNumber(false, false, false, null));
        map.put("liked", new EntityObject(false, true, false, null, "TagAssetLike"));
        map.put("name", new EntityString(false, false, false, null));
        map.put("type", new EntityString(false, false, false, null));
        map.put("previewDetailExists", new EntityBoolean(false, false, false, "preview_detail_exists"));
        map.put("previewTileExists", new EntityBoolean(false, false, false, "preview_tile_exists"));
        String expected = "import {\n" +
                "    Decoder,\n" +
                "    boolean,\n" +
                "    field,\n" +
                "    nullable,\n" +
                "    number,\n" +
                "    object,\n" +
                "    string\n" +
                "} from 'json-bouncer'\n" +
                "\n" +
                "import {\n" +
                "    TagAssetLike,\n" +
                "    tagAssetLikeDecoder\n" +
                "} from './TagAssetLike.entity.evolve'\n" +
                "\n" +
                "export interface TagAsset {\n" +
                "    readonly id: number\n" +
                "    readonly liked: TagAssetLike | null\n" +
                "    readonly name: string\n" +
                "    readonly previewDetailExists: boolean\n" +
                "    readonly previewTileExists: boolean\n" +
                "    readonly type: string\n" +
                "}\n" +
                "\n" +
                "export function tagAssetDecoder(): Decoder<TagAsset> {\n" +
                "    return object({\n" +
                "        id: field('id', number()),\n" +
                "        liked: field('liked', nullable(tagAssetLikeDecoder())),\n" +
                "        name: field('name', string()),\n" +
                "        previewDetailExists: field('preview_detail_exists', boolean()),\n" +
                "        previewTileExists: field('preview_tile_exists', boolean()),\n" +
                "        type: field('type', string()),\n" +
                "    })\n" +
                "}";
        Rendered rendered = entityRenderer.render(new Deserialized<>(Paths.get("/tmp/tagAsset.entity.json"), map));
        assertEquals(expected, rendered.getRendered());
    }

    /**
     * Recursive file decoder
     */
    @Test
    public void renderFile() {
        Map<String, Entity> map = new HashMap<>();
        map.put("id", new EntityNumber(false, false, false, null));
        map.put("file", new EntityObject(false, false, false, null, "File"));
        String expected = "import {\n" +
                "    Decoder,\n" +
                "    field,\n" +
                "    lazy,\n" +
                "    number,\n" +
                "    object\n" +
                "} from 'json-bouncer'\n" +
                "\n" +
                "export interface File {\n" +
                "    readonly file: File\n" +
                "    readonly id: number\n" +
                "}\n" +
                "\n" +
                "export function fileDecoder(): Decoder<File> {\n" +
                "    return object({\n" +
                "        file: field('file', lazy(fileDecoder)),\n" +
                "        id: field('id', number()),\n" +
                "    })\n" +
                "}";
        Rendered rendered = entityRenderer.render(new Deserialized<>(Paths.get("/tmp/file.entity.json"), map));
        assertEquals(expected, rendered.getRendered());
    }

}
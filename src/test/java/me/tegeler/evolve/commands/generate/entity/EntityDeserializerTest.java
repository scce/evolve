package me.tegeler.evolve.commands.generate.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tegeler.evolve.AbstractResourceBasedTest;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.entity.model.Entity;
import me.tegeler.evolve.commands.generate.entity.model.EntityNumber;
import me.tegeler.evolve.commands.generate.entity.model.EntityObject;
import me.tegeler.evolve.commands.generate.entity.model.EntityString;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

public class EntityDeserializerTest extends AbstractResourceBasedTest {
    private final EntityDeserializer entityDeserializer = new EntityDeserializer(new ObjectMapper());

    @Test
    public void deserialize() throws IOException {
        String jsonInput = getTestResourceFileContent("commands/generate/entity/user.entity.json");

        Deserialized<Map<String, Entity>> deserialize = entityDeserializer.deserialize(new Validated(null, jsonInput));
        Map<String, Entity> output = deserialize.getPayload();

        assertEquals("number", output.get("age").getType().toString());
        assertEquals(EntityNumber.class, output.get("age").getClass());
        assertFalse(output.get("age").getArray());
        assertFalse(output.get("age").getNullAble());
        assertFalse(output.get("age").getOptional());
        assertNull(output.get("age").getOrigin());
        assertTrue(output.containsKey("age"));

        assertEquals("string", output.get("name").getType().toString());
        assertEquals(EntityString.class, output.get("name").getClass());
        assertFalse(output.get("name").getArray());
        assertFalse(output.get("name").getNullAble());
        assertFalse(output.get("name").getOptional());
        assertNull(output.get("name").getOrigin());
        assertTrue(output.containsKey("name"));

        assertEquals("string", output.get("lastName").getType().toString());
        assertEquals("surName", output.get("lastName").getOrigin());
        assertEquals(EntityString.class, output.get("lastName").getClass());
        assertFalse(output.get("lastName").getArray());
        assertFalse(output.get("lastName").getNullAble());
        assertTrue(output.get("lastName").getOptional());
        assertTrue(output.containsKey("lastName"));

        assertEquals("object", output.get("credentials").getType().toString());
        assertEquals(EntityObject.class, output.get("credentials").getClass());
        assertFalse(output.get("credentials").getArray());
        assertFalse(output.get("credentials").getNullAble());
        assertFalse(output.get("credentials").getOptional());
        assertNull(output.get("credentials").getOrigin());
        assertTrue(output.containsKey("credentials"));
    }

}
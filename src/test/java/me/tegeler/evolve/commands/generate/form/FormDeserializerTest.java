package me.tegeler.evolve.commands.generate.form;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tegeler.evolve.AbstractResourceBasedTest;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.form.model.Form;
import me.tegeler.evolve.commands.generate.form.model.Input;
import me.tegeler.evolve.commands.generate.form.model.InputType;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static org.junit.Assert.*;

public class FormDeserializerTest extends AbstractResourceBasedTest {

	private final FormDeserializer deserializer = new FormDeserializer(new ObjectMapper());

	@Test
	public void test() throws IOException {

		String serializedInput = this.getTestResourceFileContent("commands/generate/form/user.form.json");

		Deserialized<Form> deserialized = deserializer.deserialize(new Validated(null, serializedInput));
		Form form = deserialized.getPayload();
		Map<String, Input> map = form.getInputs();

		assertEquals(3, map.size());
		assertNull(deserialized.getPath());
		assertEquals("User", form.getPage());

		String key;
		Input input;

		//key
		key = "name";
		assertTrue(map.containsKey(key));
		input = map.get(key);
		assertEquals(InputType.STRING, input.getType());
		assertNull(input.getDefaultValue());
		assertFalse(input.getMandatory());

		//email
		key = "email";
		assertTrue(map.containsKey(key));
		input = map.get(key);
		assertEquals(InputType.EMAIL, input.getType());
		assertNull(input.getDefaultValue());
		assertTrue(input.getMandatory());

		//account
		key = "account";
		assertTrue(map.containsKey(key));
		input = map.get(key);
		assertEquals(InputType.FIXEDPOINTNUMBER, input.getType());
		assertNull(input.getDefaultValue());
		assertFalse(input.getMandatory());
	}

}

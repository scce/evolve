package me.tegeler.evolve.commands.generate;

import me.tegeler.evolve.util.FileReader;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ReaderTest {
    @Test
    public void test() throws IOException {
        //given
        FileReader fileReaderMock = mock(FileReader.class);
        Path path = new File("/tmp/ReaderTest").toPath();
        String byteArray = "byteArray";
        when(fileReaderMock.read(path)).thenReturn(byteArray.getBytes());

        //when
        Reader reader = new Reader(fileReaderMock);
        Read read = reader.read(path);

        //then
        verify(fileReaderMock, times(1)).read(path);
        assertEquals(read.getPath(), path);
        assertEquals(read.getJson(), byteArray);
    }
}
package me.tegeler.evolve.commands.generate.entity;

import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Result;
import me.tegeler.evolve.util.FileWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class EntityWriterTest {

    @Test
    public void test() throws FileNotFoundException {
        //given
        FileWriter mockedFileWriter = mock(FileWriter.class);
        EntityWriter entityWriter = new EntityWriter(mockedFileWriter);

        Path path = new File("/tmp/test").toPath();
        Rendered testString = new Rendered(path, "testString");

        //when
        Result result = entityWriter.write(testString);

        //then
        assertEquals("Generate test ✓", result.getMessage());
        verify(mockedFileWriter, times(1)).write(any(File.class), any(Rendered.class));
    }
}
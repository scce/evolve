package me.tegeler.evolve.commands.generate.api;

import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.api.model.*;
import me.tegeler.evolve.target.http.HttpRequestMethod;
import me.tegeler.evolve.target.http.HttpRequestMethodGenerator;
import me.tegeler.evolve.target.typescript.*;
import org.junit.Test;
import org.mockito.Matchers;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApiRendererTest {

    @Test
    public void test() {
        //given
        HttpRequestMethodGenerator httpRequestMethodGenerator = mock(HttpRequestMethodGenerator.class);
        ImportGenerator importGenerator = mock(ImportGenerator.class);
        when(httpRequestMethodGenerator.generate(Matchers.any(HttpRequestMethod.class))).thenReturn(new PartialGeneratorResult(true, "<GeneratedMethod>"));
        when(importGenerator.generate(Matchers.any(Import.class))).thenReturn(new PartialGeneratorResult(true, "<GeneratedImport>"));
        Api api = new Api(
                "/path",
                new Rest(
                        new Create("responseEntity", "requestEntity"),
                        new Retrieve("responseEntity"),
                        new Update("responseEntity", "requestEntity"),
                        new Delete(),
                        new List(Collections.emptyMap())
                ),
                Collections.singletonList(
                        new Method(
                                "name",
                                MethodEnum.GET,
                                "path",
                                "responseEntity",
                                "requestEntity",
                                Collections.emptyMap(),
                                null
                        )
                )
        );
        Path path = new File("/tmp/ApiRenderedTest").toPath();
        Deserialized<Api> deserialized = new Deserialized<>(path, api);

        //when
        ApiRenderer apiRenderer = new ApiRenderer(httpRequestMethodGenerator, importGenerator);
        Rendered rendered = apiRenderer.render(deserialized);

        //then
        assertEquals(path, rendered.getPath());
        String expectedGeneratedMethods = "<GeneratedImport>\n\n" +
                "<GeneratedImport>\n\n" +
                "<GeneratedMethod>\n\n" +
                "<GeneratedMethod>\n\n" +
                "<GeneratedMethod>\n\n" +
                "<GeneratedMethod>\n\n" +
                "<GeneratedMethod>\n\n" +
                "<GeneratedMethod>";
        assertEquals(expectedGeneratedMethods, rendered.getRendered());
    }
}
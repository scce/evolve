package me.tegeler.evolve.commands.generate.form;

import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Result;
import me.tegeler.evolve.util.FileWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class FormWriterTest {

    @Test
    public void test() throws FileNotFoundException {
        //given
        FileWriter mockedFileWriter = mock(FileWriter.class);
        FormWriter formWriter = new FormWriter(mockedFileWriter);

        Path path = new File("/tmp/test").toPath();
        Rendered testString = new Rendered(path, "testString");

        //when
        Result result = formWriter.write(testString);

        //then
        assertEquals("Generate test ✓", result.getMessage());
        verify(mockedFileWriter, times(1)).write(any(File.class), any(Rendered.class));
    }
}
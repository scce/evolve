package me.tegeler.evolve.commands.generate;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import me.tegeler.evolve.commands.generate.entity.EntityDeserializer;
import me.tegeler.evolve.commands.generate.entity.EntityRenderer;
import me.tegeler.evolve.commands.generate.entity.model.Entity;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ProcessorTest {
    @Test
    public void test() throws IOException, ProcessingException {
        //given
        Reader reader = mock(Reader.class);
        Validator validator = mock(Validator.class);
        EntityDeserializer deserializer = mock(EntityDeserializer.class);
        EntityRenderer renderer = mock(EntityRenderer.class);
        Writer writer = mock(Writer.class);

        Path path = new File("/tmp/processorTest").toPath();
        String json = "json";
        Read read = new Read(path, json);
        Validated validated = new Validated(path, json);
        Deserialized<Map<String, Entity>> deserialized = new Deserialized<>(path, new HashMap<>());
        Rendered rendered = new Rendered(path, "rendered");
        Result result = Result.createSuccessfulRenderedResult(rendered);

        when(reader.read(path)).thenReturn(read);
        when(validator.validate(read)).thenReturn(validated);
        when(deserializer.deserialize(validated)).thenReturn(deserialized);
        when(renderer.render(deserialized)).thenReturn(rendered);
        when(writer.write(rendered)).thenReturn(result);


        //when
        Processor processor = new Processor<>(
                deserializer,
                reader,
                renderer,
                validator,
                writer
        );
        Result actual = processor.process(path);

        //then
        assertEquals("Generate processorTest ✓", actual.getMessage());
        verify(reader, times(1)).read(path);
        verify(validator, times(1)).validate(read);
        verify(deserializer, times(1)).deserialize(validated);
        verify(renderer, times(1)).render(deserialized);
        verify(writer, times(1)).write(rendered);
    }

    @Test
    public void testNoSuccess() throws IOException, ProcessingException {
        //given
        Reader reader = mock(Reader.class);
        Validator validator = mock(Validator.class);
        EntityDeserializer deserializer = mock(EntityDeserializer.class);
        EntityRenderer renderer = mock(EntityRenderer.class);
        Writer writer = mock(Writer.class);

        Path path = new File("/tmp/processorTest").toPath();
        String json = "json";
        Read read = new Read(path, json);
        Validated validated = new Validated(path, json);
        Deserialized<Map<String, Entity>> deserialized = new Deserialized<>(path, new HashMap<>());
        Rendered rendered = new Rendered(path, "rendered");

        when(reader.read(path)).thenThrow(new IOException("IO Exception"));

        //when
        Processor processor = new Processor<>(
                deserializer,
                reader,
                renderer,
                validator,
                writer
        );
        Result result = processor.process(path);

        //then
        assertEquals("IO Exception", result.getMessage());
        verify(reader, times(1)).read(path);
        verify(validator, times(0)).validate(read);
        verify(deserializer, times(0)).deserialize(validated);
        verify(renderer, times(0)).render(deserialized);
        verify(writer, times(0)).write(rendered);
    }
}
package me.tegeler.evolve.commands.generate.redux;

import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Result;
import me.tegeler.evolve.util.FileWriter;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ReduxWriterTest {

	@Test
	public void test() throws FileNotFoundException {
		//given
		FileWriter mockedFileWriter = mock(FileWriter.class);
		ReduxWriter reduxWriter = new ReduxWriter(mockedFileWriter);

		Path path = new File("/tmp/test").toPath();
		Rendered testString = new Rendered(path, "testString");

		//when
		Result result = reduxWriter.write(testString);

		//then
		assertEquals("Generate test ✓", result.getMessage());
		verify(mockedFileWriter, times(1)).write(any(File.class), any(Rendered.class));
	}

}
package me.tegeler.evolve.commands.generate.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.api.model.*;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ApiDeserializerTest {
	private final ApiDeserializer apiDeserializer = new ApiDeserializer(new ObjectMapper());

	@Test
	public void deserialize() throws IOException {
		//given
		String jsonInput = "{\n" +
				"  \"path\": \"/users\",\n" +
				"  \"rest\": {\n" +
				"    \"createReducer\": {\n" +
				"      \"responseEntity\": \"UserResponse\",\n" +
				"      \"requestEntity\": \"UserRequest\"\n" +
				"    },\n" +
				"    \"retrieve\": {\n" +
				"      \"responseEntity\": \"UserResponse\"\n" +
				"    },\n" +
				"    \"update\": {\n" +
				"      \"responseEntity\": \"UserResponse\",\n" +
				"      \"requestEntity\": \"UserRequest\"\n" +
				"    },\n" +
				"    \"delete\": {\n" +
				"    },\n" +
				"    \"list\": {\n" +
				"    }\n" +
				"  },\n" +
				"  \"methods\": [\n" +
				"    {\n" +
				"      \"name\": \"getLogins\",\n" +
				"      \"path\": \"logins\",\n" +
				"      \"parameters\": {\n" +
				"           \"asc\":{\n" +
				"               \"type\": \"string\"\n" +
				"			}\n" +
				"        },\n" +
				"      \"method\": \"GET\",\n" +
				"      \"responseEntity\": \"Logins\"\n" +
				"    },\n" +
				"    {\n" +
				"      \"name\": \"deleteLogin\",\n" +
				"      \"path\": \"logins\",\n" +
				"      \"method\": \"DELETE\"\n" +
				"    }\n" +
				"  ]\n" +
				"}";

		//when
		Deserialized<Api> deserialized = apiDeserializer.deserialize(new Validated(null, jsonInput));

		//then
		Map<String, Parameter> parameterMap = new HashMap<>();
		parameterMap.put("asc", new Parameter(ParameterType.STRING));
		Method getLogins = new Method(
				"getLogins",
				MethodEnum.GET,
				"logins",
				"Logins",
				null,
				parameterMap,
				null
		);
		Method deleteLogins = new Method(
				"deleteLogin",
				MethodEnum.DELETE,
				"logins",
				null,
				null,
				null,
				null
		);
		List<Method> expectedMethods = new ArrayList<>(Arrays.asList(getLogins, deleteLogins));
		Api expectedApi = new Api("/users", null, expectedMethods);

		Api actualApi = deserialized.getPayload();
		assertEquals(expectedApi.getPath(), actualApi.getPath());
		assertEquals(expectedApi.getMethods().size(), actualApi.getMethods().size());
		testMethod(expectedApi, actualApi, 0);
		testMethod(expectedApi, actualApi, 1);
	}

	private void testMethod(Api expectedApi, Api actualApi, int i) {
		Method expectedMethod = expectedApi.getMethods().get(i);
		Method actualMethod = actualApi.getMethods().get(i);

		assertEquals(expectedMethod.getMethod(), actualMethod.getMethod());
		assertEquals(expectedMethod.getName(), actualMethod.getName());
		assertEquals(expectedMethod.getPath(), actualMethod.getPath());
	}

}
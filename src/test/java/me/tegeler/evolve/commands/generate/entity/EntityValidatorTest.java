package me.tegeler.evolve.commands.generate.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import me.tegeler.evolve.commands.generate.Read;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class EntityValidatorTest {

	private EntityValidator entityValidator;

	@Before
	public void setUp() {
		entityValidator = new EntityValidator(new ObjectMapper());
	}

	@Test
	public void test() throws IOException, ProcessingException {
		entityValidator.validate(new Read(new File("/tmp").toPath(), "{}"));
	}

	@Test(expected = RuntimeException.class)
	public void testRuntimeException() throws IOException, ProcessingException {
		String json = "{" +
				"\"test\": {" +
				"}" +
				"}";
		entityValidator.validate(new Read(new File("/tmp").toPath(), json));
	}

	@Test(expected = NullPointerException.class)
	public void testException() throws IOException, ProcessingException {
		entityValidator.validate(new Read(new File("/tmp").toPath(), ""));
	}

}
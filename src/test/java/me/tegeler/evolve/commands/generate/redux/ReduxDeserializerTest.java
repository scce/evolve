package me.tegeler.evolve.commands.generate.redux;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.redux.model.Redux;
import me.tegeler.evolve.commands.generate.redux.model.State;
import me.tegeler.evolve.commands.generate.redux.model.StateBoolean;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

public class ReduxDeserializerTest {

	private final ReduxDeserializer deserializer = new ReduxDeserializer(new ObjectMapper());

	@Test
	public void test() throws IOException {

		String serializedInput =
				"{\n" +
						"  \"page\": \"NavBar\",\n" +
						"  \"states\": {\n" +
						"    \"userMenu\": {\n" +
						"      \"type\": \"boolean\"\n" +
						"    },\n" +
						"    \"accountMenu\": {\n" +
						"      \"type\": \"boolean\",\n" +
						"      \"toggle\": true,\n" +
						"      \"disable\": true\n" +
						"    }\n" +
						"  }\n" +
						"}";

		Deserialized<Redux> deserialized = deserializer.deserialize(new Validated(null, serializedInput));
		Redux redux = deserialized.getPayload();
		Map<String, State> map = redux.getStates();

		assertEquals(2, map.size());
		assertNull(deserialized.getPath());
		assertEquals("NavBar", redux.getPage());

		String key;
		StateBoolean state;

		//key
		key = "userMenu";
		assertTrue(map.containsKey(key));
		state = (StateBoolean) map.get(key);
		assertEquals("boolean", state.getType());
		assertFalse(state.hasAtLeastOneAction());

		//email
		key = "accountMenu";
		assertTrue(map.containsKey(key));
		state = (StateBoolean) map.get(key);
		assertEquals("boolean", state.getType());
		assertTrue(state.hasAtLeastOneAction());
	}
}
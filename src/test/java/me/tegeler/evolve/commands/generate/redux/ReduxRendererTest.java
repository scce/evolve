package me.tegeler.evolve.commands.generate.redux;

import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.redux.model.Redux;
import me.tegeler.evolve.commands.generate.redux.model.State;
import me.tegeler.evolve.commands.generate.redux.model.StateBoolean;
import me.tegeler.evolve.target.redux.ActionBuilderGenerator;
import me.tegeler.evolve.target.redux.InitFunctionGenerator;
import me.tegeler.evolve.target.redux.ReducerGenerator;
import me.tegeler.evolve.target.typescript.ImportGenerator;
import me.tegeler.evolve.target.typescript.InterfaceGenerator;
import me.tegeler.evolve.target.typescript.UnionTypeGenerator;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ReduxRendererTest {
	private ReduxRenderer reduxRenderer;

	@Before
	public void setUp() throws IOException {
		Freemarker freemarker = new Freemarker();
		reduxRenderer = new ReduxRenderer(
				new ActionBuilderGenerator(freemarker),
				new ImportGenerator(freemarker),
				new InitFunctionGenerator(freemarker),
				new InterfaceGenerator(freemarker),
				new ReducerGenerator(freemarker),
				new UnionTypeGenerator(freemarker)
		);
	}


	@Test
	public void render() {
		//given
		Map<String, State> states = new HashMap<>();
		states.put("navigationOpen", new StateBoolean(true, true, "false"));
		Deserialized<Redux> deserialized = new Deserialized<>(
				Paths.get("/tmp/navbar.redux.json"),
				new Redux("Navbar", states)
		);

		//when
		Rendered rendered = reduxRenderer.render(deserialized);

		//then
		String expected = "" +
				"import {\n" +
				"    Reducer\n" +
				"} from 'redux'\n" +
				"\n" +
				"export interface NavbarState {\n" +
				"    readonly navigationOpen: boolean\n" +
				"}\n" +
				"\n" +
				"export function initNavbarState() {\n" +
				"    return {\n" +
				"        navigationOpen: false\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export type NavbarReduxAction =\n" +
				"    NavbarNavigationOpenDisableAction |\n" +
				"    NavbarNavigationOpenToggleAction\n" +
				"\n" +
				"interface NavbarNavigationOpenDisableAction {\n" +
				"    readonly action: 'DisableNavigationOpen'\n" +
				"    readonly page: 'Navbar'\n" +
				"    readonly type: 'NavbarRedux'\n" +
				"}\n" +
				"\n" +
				"interface NavbarNavigationOpenToggleAction {\n" +
				"    readonly action: 'ToggleNavigationOpen'\n" +
				"    readonly page: 'Navbar'\n" +
				"    readonly type: 'NavbarRedux'\n" +
				"}\n" +
				"\n" +
				"export function buildNavbarNavigationOpenDisableAction():\n" +
				"    NavbarNavigationOpenDisableAction {\n" +
				"    return {\n" +
				"        action: 'DisableNavigationOpen',\n" +
				"        page: 'Navbar',\n" +
				"        type: 'NavbarRedux',\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildNavbarNavigationOpenToggleAction():\n" +
				"    NavbarNavigationOpenToggleAction {\n" +
				"    return {\n" +
				"        action: 'ToggleNavigationOpen',\n" +
				"        page: 'Navbar',\n" +
				"        type: 'NavbarRedux',\n" +
				"    }\n" +
				"}\n" +
				"\n" +
                "export const reducerNavbar: Reducer<NavbarState> =\n" +
                "    (\n" +
				"        state: NavbarState,\n" +
				"        action: NavbarReduxAction\n" +
				"    ): NavbarState => {\n" +
                "\n" +
                "        switch (action.action) {\n" +
				"            case 'DisableNavigationOpen':\n" +
				"                return {\n" +
				"                    ...state,\n" +
				"                    navigationOpen: false\n" +
				"                }\n" +
				"            case 'ToggleNavigationOpen':\n" +
				"                return {\n" +
				"                    ...state,\n" +
				"                    navigationOpen: !state.navigationOpen\n" +
				"                }\n" +
                "        }\n" +
                "\n" +
                "    }";
		assertEquals(expected, rendered.getRendered());
	}

	/**
	 * without any action
	 */
	@Test
	public void render2() {
		//given
		Map<String, State> states = new HashMap<>();
		states.put("navigationOpen", new StateBoolean(false, false, "false"));
		Deserialized<Redux> deserialized = new Deserialized<>(
				Paths.get("/tmp/navbar.redux.json"),
				new Redux("Navbar", states)
		);

		//when
		Rendered rendered = reduxRenderer.render(deserialized);

		//then
		String expected = "" +
				"export interface NavbarState {\n" +
				"    readonly navigationOpen: boolean\n" +
				"}\n" +
				"\n" +
				"export function initNavbarState() {\n" +
				"    return {\n" +
				"        navigationOpen: false\n" +
				"    }\n" +
				"}";
		assertEquals(expected, rendered.getRendered());
	}
}
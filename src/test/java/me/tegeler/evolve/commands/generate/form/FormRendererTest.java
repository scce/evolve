package me.tegeler.evolve.commands.generate.form;

import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.form.model.*;
import me.tegeler.evolve.target.form.*;
import me.tegeler.evolve.target.typescript.ImportGenerator;
import me.tegeler.evolve.target.typescript.InterfaceGenerator;
import me.tegeler.evolve.target.typescript.UnionTypeGenerator;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FormRendererTest {
	private FormRenderer formRenderer;

	public FormRendererTest() throws IOException {
		Freemarker freemarker = new Freemarker();
		formRenderer = new FormRenderer(
				new ImportGenerator(freemarker),
				new InterfaceGenerator(freemarker),
				new UnionTypeGenerator(freemarker),
				new ChangeFunctionGenerator(freemarker),
				new ReducerGenerator(freemarker),
				new SettingsGenerator(freemarker),
				new ValidatorsGenerator(freemarker),
				new ValidateFunctionGenerator(freemarker)
		);
	}

	/**
	 * page is upper case
	 */
	@Test
	public void testRender() {
		check(new Form("User", getInput()));
	}

	/**
	 * page is lower case
	 */
	@Test
	public void testRender2() {
		check(new Form("user", getInput()));
	}

	private Map<String, Input> getInput() {
		Map<String, Input> map = new HashMap<>();
		map.put("birthday", new InputDate(null, true));
		map.put("activeUntil", new InputDatetime(null, true));
		map.put("age", new InputInt(null, true));
		map.put("balance", new InputFixedPointNumber(null, true));
		map.put("female", new InputBoolean(null, true));
		map.put("name", new InputString(null, true));
		map.put("nickname", new InputString(null, false));
		map.put("email", new InputEmail(null, true));
		return map;
	}

	private void check(Form form) {
		String expected = getExpected();
		Rendered rendered = formRenderer.render(new Deserialized<>(Paths.get("/tmp/User.form.json"), form));
		assertEquals(expected, rendered.getRendered());
	}

	private String getExpected() {
		return "import {\n" +
				"    Result,\n" +
				"    compose,\n" +
				"    emailValidator,\n" +
				"    errorToNullable,\n" +
				"    mandatoryNullableValidator,\n" +
				"    mandatoryStringValidator,\n" +
				"    stringToFixedPointNumberValidator,\n" +
				"    stringToIntValidator,\n" +
				"    succeedValidator\n" +
				"} from 'input-bouncer'\n" +
				"\n" +
				"import {\n" +
				"    LocalDate,\n" +
				"    LocalDateTime\n" +
				"} from 'js-joda'\n" +
				"\n" +
				"export interface UserFormState {\n" +
				"    readonly input: UserFormInput\n" +
				"    readonly submissionAttempted: boolean\n" +
				"    readonly validation: UserFormValidation\n" +
				"}\n" +
				"\n" +
				"export interface UserFormInput {\n" +
				"    readonly activeUntil: LocalDateTime | null\n" +
				"    readonly age: string\n" +
				"    readonly balance: string\n" +
				"    readonly birthday: LocalDate | null\n" +
				"    readonly email: string\n" +
				"    readonly female: boolean\n" +
				"    readonly name: string\n" +
				"    readonly nickname: string\n" +
				"}\n" +
				"\n" +
				"export interface UserFormValidation {\n" +
				"    readonly activeUntil: 'Mandatory' | null\n" +
				"    readonly age: 'Mandatory' | 'InvalidInt' | null\n" +
				"    readonly balance: 'Mandatory' | 'InvalidFixedPointNumber' | null\n" +
				"    readonly birthday: 'Mandatory' | null\n" +
				"    readonly email: 'Mandatory' | 'InvalidEmail' | null\n" +
				"    readonly female: 'Mandatory' | null\n" +
				"    readonly name: 'Mandatory' | null\n" +
				"    readonly nickname: null\n" +
				"}\n" +
				"\n" +
				"export interface UserFormData {\n" +
				"    readonly activeUntil: LocalDateTime\n" +
				"    readonly age: number\n" +
				"    readonly balance: number\n" +
				"    readonly birthday: LocalDate\n" +
				"    readonly email: string\n" +
				"    readonly female: boolean\n" +
				"    readonly name: string\n" +
				"    readonly nickname: string | null\n" +
				"}\n" +
				"\n" +
				"export type UserFormAction =\n" +
				"    UserFormChangeActiveUntilAction |\n" +
				"    UserFormChangeAgeAction |\n" +
				"    UserFormChangeBalanceAction |\n" +
				"    UserFormChangeBirthdayAction |\n" +
				"    UserFormChangeEmailAction |\n" +
				"    UserFormChangeFemaleAction |\n" +
				"    UserFormChangeNameAction |\n" +
				"    UserFormChangeNicknameAction\n" +
				"\n" +
				"interface UserFormChangeActiveUntilAction {\n" +
				"    readonly action: 'ChangeActiveUntil'\n" +
				"    readonly activeUntil: LocalDateTime | null\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeAgeAction {\n" +
				"    readonly action: 'ChangeAge'\n" +
				"    readonly age: string\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeBalanceAction {\n" +
				"    readonly action: 'ChangeBalance'\n" +
				"    readonly balance: string\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeBirthdayAction {\n" +
				"    readonly action: 'ChangeBirthday'\n" +
				"    readonly birthday: LocalDate | null\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeEmailAction {\n" +
				"    readonly action: 'ChangeEmail'\n" +
				"    readonly email: string\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeFemaleAction {\n" +
				"    readonly action: 'ChangeFemale'\n" +
				"    readonly female: boolean\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeNameAction {\n" +
				"    readonly action: 'ChangeName'\n" +
				"    readonly name: string\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"interface UserFormChangeNicknameAction {\n" +
				"    readonly action: 'ChangeNickname'\n" +
				"    readonly nickname: string\n" +
				"    readonly page: 'User'\n" +
				"    readonly type: 'UserForm'\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeActiveUntilAction(\n" +
				"    activeUntil: LocalDateTime | null\n" +
				"): UserFormChangeActiveUntilAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeActiveUntil',\n" +
				"        activeUntil: activeUntil\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeAgeAction(\n" +
				"    age: string\n" +
				"): UserFormChangeAgeAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeAge',\n" +
				"        age: age\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeBalanceAction(\n" +
				"    balance: string\n" +
				"): UserFormChangeBalanceAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeBalance',\n" +
				"        balance: balance\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeBirthdayAction(\n" +
				"    birthday: LocalDate | null\n" +
				"): UserFormChangeBirthdayAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeBirthday',\n" +
				"        birthday: birthday\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeEmailAction(\n" +
				"    email: string\n" +
				"): UserFormChangeEmailAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeEmail',\n" +
				"        email: email\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeFemaleAction(\n" +
				"    female: boolean\n" +
				"): UserFormChangeFemaleAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeFemale',\n" +
				"        female: female\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeNameAction(\n" +
				"    name: string\n" +
				"): UserFormChangeNameAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeName',\n" +
				"        name: name\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function buildUserFormChangeNicknameAction(\n" +
				"    nickname: string\n" +
				"): UserFormChangeNicknameAction {\n" +
				"    return {\n" +
				"        page: 'User',\n" +
				"        type: 'UserForm',\n" +
				"        action: 'ChangeNickname',\n" +
				"        nickname: nickname\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export function userFormReducer(\n" +
				"    state: UserFormState,\n" +
				"    action: UserFormAction\n" +
				"): UserFormState {\n" +
				"    switch (action.action) {\n" +
				"        case 'ChangeActiveUntil':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    activeUntil: action.activeUntil\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeAge':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    age: action.age\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeBalance':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    balance: action.balance\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeBirthday':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    birthday: action.birthday\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeEmail':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    email: action.email\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeFemale':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    female: action.female\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeName':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    name: action.name\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangeNickname':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    nickname: action.nickname\n" +
				"                }\n" +
				"            }\n" +
				"\n" +
				"    }\n" +
				"}\n" +
				"\n" +
				"export const userFormSettings = {\n" +
				"    activeUntil: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    age: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    balance: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    birthday: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    email: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    female: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    name: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    nickname: {\n" +
				"        mandatory: false\n" +
				"    },\n" +
				"\n" +
				"}\n" +
				"\n" +
				"const validators = {\n" +
				"    activeUntil: mandatoryNullableValidator(),\n" +
				"    age: compose(stringToIntValidator(), mandatoryStringValidator()),\n" +
				"    balance: compose(stringToFixedPointNumberValidator(2), mandatoryStringValidator()),\n" +
				"    birthday: mandatoryNullableValidator(),\n" +
				"    email: compose(emailValidator(), mandatoryStringValidator()),\n" +
				"    female: succeedValidator(),\n" +
				"    name: mandatoryStringValidator(),\n" +
				"    nickname: succeedValidator(),\n" +
				"\n" +
				"}\n" +
				"\n" +
				"export function validateUserForm(\n" +
				"    input: UserFormInput\n" +
				"): Result<UserFormValidation, UserFormData> {\n" +
				"    const validation = {\n" +
				"        activeUntil: validators.activeUntil(input.activeUntil),\n" +
				"        age: validators.age(input.age),\n" +
				"        balance: validators.balance(input.balance),\n" +
				"        birthday: validators.birthday(input.birthday),\n" +
				"        email: validators.email(input.email),\n" +
				"        female: validators.female(input.female),\n" +
				"        name: validators.name(input.name),\n" +
				"        nickname: validators.nickname(input.nickname),\n" +
				"\n" +
				"    }\n" +
				"    if (\n" +
				"        validation.activeUntil.type === 'Ok'\n" +
				"        && validation.age.type === 'Ok'\n" +
				"        && validation.balance.type === 'Ok'\n" +
				"        && validation.birthday.type === 'Ok'\n" +
				"        && validation.email.type === 'Ok'\n" +
				"        && validation.female.type === 'Ok'\n" +
				"        && validation.name.type === 'Ok'\n" +
				"        && validation.nickname.type === 'Ok'\n" +
				"\n" +
				"    ) {\n" +
				"        return {\n" +
				"            type: 'Ok',\n" +
				"            value: {\n" +
				"                activeUntil: validation.activeUntil.value,\n" +
				"                age: validation.age.value,\n" +
				"                balance: validation.balance.value,\n" +
				"                birthday: validation.birthday.value,\n" +
				"                email: validation.email.value,\n" +
				"                female: validation.female.value,\n" +
				"                name: validation.name.value,\n" +
				"                nickname: validation.nickname.value,\n" +
				"\n" +
				"            },\n" +
				"        }\n" +
				"    } else {\n" +
				"        return {\n" +
				"            type: 'Err',\n" +
				"            error: {\n" +
				"                activeUntil: errorToNullable(validation.activeUntil),\n" +
				"                age: errorToNullable(validation.age),\n" +
				"                balance: errorToNullable(validation.balance),\n" +
				"                birthday: errorToNullable(validation.birthday),\n" +
				"                email: errorToNullable(validation.email),\n" +
				"                female: errorToNullable(validation.female),\n" +
				"                name: errorToNullable(validation.name),\n" +
				"                nickname: errorToNullable(validation.nickname),\n" +
				"\n" +
				"            },\n" +
				"        }\n" +
				"    }\n" +
				"}\n" +
				"";
	}

}
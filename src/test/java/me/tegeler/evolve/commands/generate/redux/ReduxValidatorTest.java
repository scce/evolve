package me.tegeler.evolve.commands.generate.redux;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import me.tegeler.evolve.commands.generate.Read;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ReduxValidatorTest {

	private ReduxValidator reduxValidator;

	@Before
	public void setUp() {
		reduxValidator = new ReduxValidator(new ObjectMapper());
	}

	@Test
	public void test() throws IOException, ProcessingException {
		String json = "{" +
				"\"page\":\"page\"," +
				"\"states\": {" +
				"}" +
				"}";
		reduxValidator.validate(new Read(new File("/tmp").toPath(), json));
	}

	@Test(expected = RuntimeException.class)
	public void testRuntimeException() throws IOException, ProcessingException {
		reduxValidator.validate(new Read(new File("/tmp").toPath(), "{}"));
	}

	@Test(expected = NullPointerException.class)
	public void testException() throws IOException, ProcessingException {
		reduxValidator.validate(new Read(new File("/tmp").toPath(), ""));
	}

}

package me.tegeler.evolve.commands.generate.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import me.tegeler.evolve.commands.generate.Read;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class ApiValidatorTest {

	private ApiValidator apiValidator;

	@Before
	public void setUp() {
		apiValidator = new ApiValidator(new ObjectMapper());
	}

	@Test
	public void test() throws IOException, ProcessingException {
		String json = "{" +
				"\"path\":\"path\"" +
				"}";
		apiValidator.validate(new Read(new File("/tmp").toPath(), json));
	}

	@Test(expected = RuntimeException.class)
	public void testRuntimeException() throws IOException, ProcessingException {
		apiValidator.validate(new Read(new File("/tmp").toPath(), "{}"));
	}

	@Test(expected = NullPointerException.class)
	public void testNullPointerException() throws IOException, ProcessingException {
		apiValidator.validate(new Read(new File("/tmp").toPath(), ""));
	}

}
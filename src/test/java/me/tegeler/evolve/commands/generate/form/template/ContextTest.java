package me.tegeler.evolve.commands.generate.form.template;

import me.tegeler.evolve.commands.generate.form.model.*;
import me.tegeler.evolve.commands.generate.form.model.Input;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ContextTest {
    @Test
    public void test() {
        Map<String, Input> map = new HashMap<>();
        map.put("age", new InputInt(null, true));
        map.put("birthday", new InputDate(null, true));
        map.put("accessUntil", new InputDatetime(null, true));
        Form form = new Form("page", map);

        Context actualContext = new Context("test", form);
        assertEquals("Test", actualContext.getNameUpperCase());
        assertEquals("test", actualContext.getNameLowerCase());

        assertEquals(map.size(), actualContext.getInputs().length);
    }

    @Test
    public void testForEmptyJsJodaImports() {
        Map<String, Input> map = new HashMap<>();
        map.put("age", new InputInt(null, false));
        Form form = new Form("page", map);

        Context actualContext = new Context("test", form);
        assertEquals("Test", actualContext.getNameUpperCase());
        assertEquals("test", actualContext.getNameLowerCase());

        assertEquals(map.size(), actualContext.getInputs().length);
    }
}
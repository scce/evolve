package me.tegeler.evolve.commands.generate.form.template;

import me.tegeler.evolve.commands.generate.form.model.*;
import org.junit.Test;

import java.util.AbstractMap;

import static org.junit.Assert.assertEquals;

public class InputTest {
    @Test
    public void testFormBoolean() {
        InputBoolean formBoolean = new InputBoolean(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formBoolean);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("boolean", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("boolean", input.getInputType());
        assertEquals("'Mandatory' | null", input.getValidationType());
        assertEquals("succeedValidator()", input.getValidator());
    }

    @Test
    public void testFormString() {
        InputString formString = new InputString(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formString);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("string", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("string", input.getInputType());
        assertEquals("'Mandatory' | null", input.getValidationType());
        assertEquals("mandatoryStringValidator()", input.getValidator());
    }

    @Test
    public void testFormInt() {
        InputInt formInt = new InputInt(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formInt);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("number", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("string", input.getInputType());
        assertEquals("'Mandatory' | 'InvalidInt' | null", input.getValidationType());
        assertEquals("compose(stringToIntValidator(), mandatoryStringValidator())", input.getValidator());
    }

    @Test
    public void testFormFixedPointNumber() {
        InputFixedPointNumber formFixedPointNumber = new InputFixedPointNumber(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formFixedPointNumber);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("number", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("string", input.getInputType());
        assertEquals("'Mandatory' | 'InvalidFixedPointNumber' | null", input.getValidationType());
        assertEquals("compose(stringToFixedPointNumberValidator(2), mandatoryStringValidator())", input.getValidator());
    }

    @Test
    public void testFormDate() {
        InputDate formDate = new InputDate(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formDate);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("LocalDate", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("LocalDate | null", input.getInputType());
        assertEquals("'Mandatory' | null", input.getValidationType());
        assertEquals("mandatoryNullableValidator()", input.getValidator());
    }

    @Test
    public void testFormDatetime() {
        InputDatetime formDatetime = new InputDatetime(null, true);
        AbstractMap.SimpleEntry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry = new AbstractMap.SimpleEntry<>("test", formDatetime);

        Input input = new Input(entry, "page");

        assertEquals("Page", input.getPage());
        assertEquals("LocalDateTime", input.getDataType());
        assertEquals("test", input.getInputNameLowerCase());
        assertEquals("Test", input.getInputNameUpperCase());
        assertEquals("LocalDateTime | null", input.getInputType());
        assertEquals("'Mandatory' | null", input.getValidationType());
        assertEquals("mandatoryNullableValidator()", input.getValidator());
    }
}
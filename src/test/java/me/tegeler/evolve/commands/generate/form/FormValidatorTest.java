package me.tegeler.evolve.commands.generate.form;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import me.tegeler.evolve.commands.generate.Read;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FormValidatorTest {

	private FormValidator formValidator;

	@Before
	public void setUp() {
		formValidator = new FormValidator(new ObjectMapper());
	}

	@Test
	public void test() throws IOException, ProcessingException {
		String json = "{" +
				"\"page\":\"page\"," +
				"\"inputs\": {" +
				"}" +
				"}";
		formValidator.validate(new Read(new File("/tmp").toPath(), json));
	}

	@Test(expected = RuntimeException.class)
	public void testRuntimeException() throws IOException, ProcessingException {
		formValidator.validate(new Read(new File("/tmp").toPath(), "{}"));
	}

	@Test(expected = NullPointerException.class)
	public void testException() throws IOException, ProcessingException {
		formValidator.validate(new Read(new File("/tmp").toPath(), ""));
	}

}
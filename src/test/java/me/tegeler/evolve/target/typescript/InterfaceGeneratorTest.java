package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InterfaceGeneratorTest {

    @Test
    public void testSuccess() throws IOException {
        //given
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("name", "string"));
        attributes.add(new Attribute("age", "number"));
        Interface anInterface = new Interface("user", attributes, true);

        //when
        InterfaceGenerator interfaceGenerator = new InterfaceGenerator(new Freemarker());
        PartialGeneratorResult actualResult = interfaceGenerator.generate(anInterface);

        //then
        String expectedResult = "export interface User {\n" +
                "    readonly age: number\n" +
                "    readonly name: string\n" +
                "}";
        assertEquals(expectedResult, actualResult.getResult());
        assertTrue(actualResult.isSuccess());
    }

}
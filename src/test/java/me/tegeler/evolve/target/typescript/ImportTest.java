package me.tegeler.evolve.target.typescript;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ImportTest {

	@Test
	public void testEqualsTrue() {
		Set<String> components1 = new HashSet<>();
		components1.add("LocalDate");
		components1.add("LocalDateTime");
		Import anImport1 = new Import(components1, "js-joda");

		Set<String> components2 = new HashSet<>();
		components2.add("LocalDate");
		components2.add("LocalDateTime");
		Import anImport2 = new Import(components2, "js-joda");

		assertEquals(anImport1, anImport2);

	}

	@Test
	public void testNotEquals() {
		Set<String> components1 = new HashSet<>();
		components1.add("LocalDate");
		components1.add("LocalDateTime");
		Import anImport1 = new Import(components1, "js-joda1");

		Set<String> components2 = new HashSet<>();
		components2.add("LocalDate");
		components2.add("LocalDateTime");
		Import anImport2 = new Import(components2, "js-joda2");

		assertNotEquals(anImport1, anImport2);

		String notAnImport = "notAnImport";
		assertNotEquals(anImport1, notAnImport);
	}

	@Test
	public void testHashCodeEquals() {
		Set<String> components1 = new HashSet<>();
		components1.add("LocalDate");
		components1.add("LocalDateTime");
		Import anImport1 = new Import(components1, "js-joda");

		Set<String> components2 = new HashSet<>();
		components2.add("LocalDate");
		components2.add("LocalDateTime");
		Import anImport2 = new Import(components2, "js-joda");

		assertEquals(anImport1.hashCode(), anImport2.hashCode());

		components1 = new HashSet<>();
		components1.add("LocalDate");
		components1.add("LocalDateTime");
		anImport1 = new Import(components1, "js-joda");

		components2 = new HashSet<>();
		components2.add("LocalDateTime");
		components2.add("LocalDate");
		anImport2 = new Import(components2, "js-joda");

		assertEquals(anImport1.hashCode(), anImport2.hashCode());
	}

	@Test
	public void testHashCodeNotEquals() {
		Set<String> components1 = new HashSet<>();
		components1.add("LocalDate");
		components1.add("LocalDateTime");
		Import anImport1 = new Import(components1, "js-joda1");

		Set<String> components2 = new HashSet<>();
		components2.add("LocalDate");
		components2.add("LocalDateTime");
		Import anImport2 = new Import(components2, "js-joda2");

		assertNotEquals(anImport1.hashCode(), anImport2.hashCode());
	}
}
package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class ImportGeneratorTest {

    @Test
    public void testSuccess() throws IOException {
        //given
        Set<String> components = new HashSet<>();
        components.add("LocalDateTime");
        components.add("LocalDate");
        Import anImport = new Import(components, "js-joda");

        //when
        ImportGenerator importGenerator = new ImportGenerator(new Freemarker());
        PartialGeneratorResult actualResult = importGenerator.generate(anImport);

        //then
        String expectedResult = "import {\n" +
                "    LocalDate,\n" +
                "    LocalDateTime\n" +
                "} from 'js-joda'";
        assertEquals(expectedResult, actualResult.getResult());
		assertTrue(actualResult.isSuccess());
    }

    @Test
    public void testNoSuccess() throws IOException {
        //given
        Set<String> components = new HashSet<>();
        Import anImport = new Import(components, null);

        //when
        ImportGenerator importGenerator = new ImportGenerator(new Freemarker());
        PartialGeneratorResult actualResult = importGenerator.generate(anImport);

        //then
		assertNull(actualResult.getResult());
		assertFalse(actualResult.isSuccess());
    }
}
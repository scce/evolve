package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UnionTypeGeneratorTest {


    @Test
    public void testSuccess() throws IOException {
        //given
        List<String> subTypes = new ArrayList<>();
        subTypes.add("UserFormChangeActiveUntilAction");
        subTypes.add("UserFormChangeAgeAction");
        subTypes.add("UserFormChangeBalanceAction");
        subTypes.add("UserFormChangeBirthdayAction");
        subTypes.add("UserFormChangeFemaleAction");
        subTypes.add("UserFormChangeNameAction");
        subTypes.add("UserFormChangeNicknameAction");
        UnionType anUnionType = new UnionType("UserFormAction", subTypes);

        //when
        UnionTypeGenerator unionTypeGenerator = new UnionTypeGenerator(new Freemarker());
        PartialGeneratorResult actualResult = unionTypeGenerator.generate(anUnionType);

        //then
        String expectedResult = "export type UserFormAction =\n" +
                "    UserFormChangeActiveUntilAction |\n" +
                "    UserFormChangeAgeAction |\n" +
                "    UserFormChangeBalanceAction |\n" +
                "    UserFormChangeBirthdayAction |\n" +
                "    UserFormChangeFemaleAction |\n" +
                "    UserFormChangeNameAction |\n" +
                "    UserFormChangeNicknameAction";
        assertEquals(expectedResult, actualResult.getResult());
		assertTrue(actualResult.isSuccess());
    }

}
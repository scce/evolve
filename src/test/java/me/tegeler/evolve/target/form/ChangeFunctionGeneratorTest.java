package me.tegeler.evolve.target.form;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ChangeFunctionGeneratorTest {

    @Test
    public void testSuccess() throws IOException {
        //given
        ChangeFunction validators = new ChangeFunction(
                "action",
                "builderFunctionName",
                "changeValue",
                "inputType",
                "page",
                "actionType",
                "type"
        );

        //when
        ChangeFunctionGenerator validatorsGenerator = new ChangeFunctionGenerator(new Freemarker());
        PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

        //then
        String expectedResult = "export function builderFunctionName(\n" +
                "    changeValue: inputType\n" +
                "): actionType {\n" +
                "    return {\n" +
                "        page: 'page',\n" +
                "        type: 'type',\n" +
                "        action: 'action',\n" +
                "        changeValue: changeValue\n" +
                "    }\n" +
                "}";
        assertTrue(actualResult.isSuccess());
        assertEquals(expectedResult, actualResult.getResult());
    }

    @Test
    public void testNoSuccess() throws IOException {
        //given
        ChangeFunction validators = new ChangeFunction(
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        //when
        ChangeFunctionGenerator validatorsGenerator = new ChangeFunctionGenerator(new Freemarker());
        PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

        //then
        assertNull(actualResult.getResult());
        assertFalse(actualResult.isSuccess());
    }
}
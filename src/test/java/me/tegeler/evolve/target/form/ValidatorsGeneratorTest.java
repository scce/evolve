package me.tegeler.evolve.target.form;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ValidatorsGeneratorTest {

    @Test
    public void testSuccess() throws IOException {
        //given
        ArrayList<Validator> validators1 = new ArrayList<>();
        validators1.add(new Validator("password", "mandatoryStringValidator()"));
        Validators validators = new Validators(validators1);

        //when
        ValidatorsGenerator validatorsGenerator = new ValidatorsGenerator(new Freemarker());
        PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

        //then
        String expectedResult = "const validators = {\n" +
                "    password: mandatoryStringValidator(),\n" +
                "\n" +
                "}";
        assertTrue(actualResult.isSuccess());
        assertEquals(expectedResult, actualResult.getResult());
    }

    @Test
    public void testNoSuccess() throws IOException {
        //given
        Validators validators = new Validators(null);

        //when
        ValidatorsGenerator validatorsGenerator = new ValidatorsGenerator(new Freemarker());
        PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

        //then
        assertNull(actualResult.getResult());
        assertFalse(actualResult.isSuccess());
    }
}
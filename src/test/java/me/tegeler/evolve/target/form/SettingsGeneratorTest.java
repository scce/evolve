package me.tegeler.evolve.target.form;

import me.tegeler.evolve.target.typescript.Language;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class SettingsGeneratorTest {

	@Test
	public void testSuccess() throws IOException {
		//given
		ArrayList<Setting> settingList = new ArrayList<>();
		settingList.add(new Setting("email", Language.getTrue()));
		settingList.add(new Setting("password", Language.getFalse()));
		Settings settings = new Settings(
				"loginFormSettings",
				settingList
		);

		//when
		SettingsGenerator settingsGenerator = new SettingsGenerator(new Freemarker());
		PartialGeneratorResult actualResult = settingsGenerator.generate(settings);

		//then
		String expectedResult = "export const loginFormSettings = {\n" +
				"    email: {\n" +
				"        mandatory: true\n" +
				"    },\n" +
				"    password: {\n" +
				"        mandatory: false\n" +
				"    },\n" +
				"\n" +
				"}";
		assertTrue(actualResult.isSuccess());
		assertEquals(expectedResult, actualResult.getResult());
	}

	@Test
	public void testNoSuccess() throws IOException {
		//given
		Settings settings = new Settings(
				null,
				null
		);

		//when
		SettingsGenerator settingsGenerator = new SettingsGenerator(new Freemarker());
		PartialGeneratorResult actualResult = settingsGenerator.generate(settings);

		//then
		assertNull(actualResult.getResult());
		assertFalse(actualResult.isSuccess());
	}
}
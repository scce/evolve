package me.tegeler.evolve.target.form;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class ReducerGeneratorTest {
	@Test
	public void testSuccess() throws IOException {
		//given
		ArrayList<Case> caseList = new ArrayList<>();
		caseList.add(new Case("email", "ChangeEmail"));
		caseList.add(new Case("password", "ChangePassword"));
		Reducer validators = new Reducer(
				"loginFormReducer",
				"LoginFormState",
				"LoginFormAction",
				"LoginFormState",
				caseList
		);

		//when
		ReducerGenerator validatorsGenerator = new ReducerGenerator(new Freemarker());
		PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

		//then
		String expectedResult = "export function loginFormReducer(\n" +
				"    state: LoginFormState,\n" +
				"    action: LoginFormAction\n" +
				"): LoginFormState {\n" +
				"    switch (action.action) {\n" +
				"        case 'ChangeEmail':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    email: action.email\n" +
				"                }\n" +
				"            }\n" +
				"        case 'ChangePassword':\n" +
				"            return {\n" +
				"                ...state,\n" +
				"                input: {\n" +
				"                    ...state.input,\n" +
				"                    password: action.password\n" +
				"                }\n" +
				"            }\n" +
				"\n" +
				"    }\n" +
				"}";
		assertTrue(actualResult.isSuccess());
		assertEquals(expectedResult, actualResult.getResult());
	}

	@Test
	public void testNoSuccess() throws IOException {
		//given
		Reducer validators = new Reducer(
				null,
				null,
				null,
				null,
				null
		);

		//when
		ReducerGenerator validatorsGenerator = new ReducerGenerator(new Freemarker());
		PartialGeneratorResult actualResult = validatorsGenerator.generate(validators);

		//then
		assertNull(actualResult.getResult());
		assertFalse(actualResult.isSuccess());
	}

}
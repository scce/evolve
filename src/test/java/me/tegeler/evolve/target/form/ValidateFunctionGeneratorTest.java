package me.tegeler.evolve.target.form;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ValidateFunctionGeneratorTest {

	@Test
	public void testSuccess() throws IOException {
		//given
		List<ValidateItem> validateItemList = new ArrayList<>();
		validateItemList.add(new ValidateItem("email"));
		validateItemList.add(new ValidateItem("password"));

		ValidateFunction validateFunction = new ValidateFunction(
				"validateLoginForm",
				"LoginFormInput",
				"LoginFormValidation",
				"LoginFormData",
				validateItemList
		);

		//when
		ValidateFunctionGenerator validateFunctionGenerator = new ValidateFunctionGenerator(new Freemarker());
		PartialGeneratorResult actualResult = validateFunctionGenerator.generate(validateFunction);

		//then
		String expectedResult = "export function validateLoginForm(\n" +
				"    input: LoginFormInput\n" +
				"): Result<LoginFormValidation, LoginFormData> {\n" +
				"    const validation = {\n" +
				"        email: validators.email(input.email),\n" +
				"        password: validators.password(input.password),\n" +
				"\n" +
				"    }\n" +
				"    if (\n" +
				"        validation.email.type === 'Ok'\n" +
				"        && validation.password.type === 'Ok'\n" +
				"\n" +
				"    ) {\n" +
				"        return {\n" +
				"            type: 'Ok',\n" +
				"            value: {\n" +
				"                email: validation.email.value,\n" +
				"                password: validation.password.value,\n" +
				"\n" +
				"            },\n" +
				"        }\n" +
				"    } else {\n" +
				"        return {\n" +
				"            type: 'Err',\n" +
				"            error: {\n" +
				"                email: errorToNullable(validation.email),\n" +
				"                password: errorToNullable(validation.password),\n" +
				"\n" +
				"            },\n" +
				"        }\n" +
				"    }\n" +
				"}\n";
		assertTrue(actualResult.isSuccess());
		assertEquals(expectedResult, actualResult.getResult());
	}

	@Test
	public void testNoSuccess() throws IOException {
		//given
		ValidateFunction validateFunction = new ValidateFunction(
				null,
				null,
				null,
				null,
				null
		);

		//when
		ValidateFunctionGenerator validateFunctionGenerator = new ValidateFunctionGenerator(new Freemarker());
		PartialGeneratorResult actualResult = validateFunctionGenerator.generate(validateFunction);

		//then
		assertNull(actualResult.getResult());
		assertFalse(actualResult.isSuccess());
	}
}
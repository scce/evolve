package me.tegeler.evolve.target.bouncer;

import me.tegeler.evolve.target.bouncer.json.Decoder;
import me.tegeler.evolve.target.bouncer.json.DecoderField;
import me.tegeler.evolve.target.bouncer.json.DecoderGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DecoderGeneratorTest {
    @Test
    public void testSuccess() throws IOException {
        //given
        List<DecoderField> decoderFields = new ArrayList<>();
        decoderFields.add(new DecoderField("groups", "groups", "array(string())"));
        decoderFields.add(new DecoderField("firstName", "first_name", "string()"));
        Decoder anDecoder = new Decoder("user", decoderFields);

        //when
        DecoderGenerator importGenerator = new DecoderGenerator(new Freemarker());
        PartialGeneratorResult actualResult = importGenerator.generate(anDecoder);

        //then
        String expectedResult = "export function userDecoder(): Decoder<User> {\n" +
                "    return object({\n" +
                "        firstName: field('first_name', string()),\n" +
                "        groups: field('groups', array(string())),\n" +
                "    })\n" +
                "}";
        assertEquals(expectedResult, actualResult.getResult());
		assertTrue(actualResult.isSuccess());
    }

}
package me.tegeler.evolve.target.http;

import me.tegeler.evolve.commands.generate.api.model.*;
import me.tegeler.evolve.commands.generate.api.model.Parameter;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class HttpRequestMethodGeneratorTest {
	@Test
	public void testCreate() throws IOException {
		//given
		Create create = new Create("responseEntity", "UserRequest");
		HttpRequestMethod method = create.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function createUserRequest(\n" +
				"   requestEntity: UserRequest\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users`,\n" +
				"        parameters: {\n" +
				"            requestEntity: requestEntity\n" +
				"        },\n" +
				"        method: 'POST',\n" +
				"        body: JSON.stringify(requestEntity),\n" +
				"        headers: {\n" +
				"            'Accept': 'application/json',\n" +
				"            'Content-Type': 'application/json',\n" +
				"        }\n" +
				"    }\n" +
				"}";
		assertEquals(expected, actual.getResult());
	}

	@Test
	public void testRetrieve() throws IOException {
		//given
		Retrieve retrieve = new Retrieve("responseEntity");
		HttpRequestMethod method = retrieve.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function retrieveUserRequest(\n" +
				"   id: number\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users/{id}`,\n" +
				"        parameters: {\n" +
				"            id: id\n" +
				"        },\n" +
				"        method: 'GET',\n" +
				"        body: null,\n" +
				"        headers: {\n" +
				"            'Accept': 'application/json',\n" +
				"        }\n" +
				"    }\n" +
				"}";

		assertEquals(expected, actual.getResult());
		assertTrue(actual.isSuccess());
	}

	@Test
	public void testUpdate() throws IOException {
		//given
		Update update = new Update("responseEntity", "UserRequest");
		HttpRequestMethod method = update.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function updateUserRequest(\n" +
				"   id: number,\n" +
				"   requestEntity: UserRequest\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users/{id}`,\n" +
				"        parameters: {\n" +
				"            id: id,\n" +
				"            requestEntity: requestEntity\n" +
				"        },\n" +
				"        method: 'PUT',\n" +
				"        body: JSON.stringify(requestEntity),\n" +
				"        headers: {\n" +
				"            'Accept': 'application/json',\n" +
				"            'Content-Type': 'application/json',\n" +
				"        }\n" +
				"    }\n" +
				"}";

		assertEquals(expected, actual.getResult());
		assertTrue(actual.isSuccess());
	}

	@Test
	public void testDelete() throws IOException {
		//given
		Delete delete = new Delete();
		HttpRequestMethod method = delete.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function deleteUserRequest(\n" +
				"   id: number\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users/{id}`,\n" +
				"        parameters: {\n" +
				"            id: id\n" +
				"        },\n" +
				"        method: 'DELETE',\n" +
				"        body: null,\n" +
				"        headers: {\n" +
				"        }\n" +
				"    }\n" +
				"}";

		assertEquals(expected, actual.getResult());
		assertTrue(actual.isSuccess());
	}

	@Test
	public void testMethod() throws IOException {
		Map<String, me.tegeler.evolve.commands.generate.api.model.Parameter> parameterMap = new HashMap<>();
		parameterMap.put("offset", new Parameter(ParameterType.NUMBER));
		parameterMap.put("limit", new Parameter(ParameterType.NUMBER));
		//given
		Method method1 = new Method(
				"downloads",
				MethodEnum.GET,
				"downloads",
				"DownloadResponse",
				"DownloadRequest",
				parameterMap,
				new ArrayList<>()
		);
		HttpRequestMethod method = method1.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function downloadsRequest(\n" +
				"   offset: number,\n" +
				"   limit: number,\n" +
				"   requestEntity: DownloadRequest\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users/downloads`,\n" +
				"        parameters: {\n" +
				"            offset: offset,\n" +
				"            limit: limit,\n" +
				"            requestEntity: requestEntity\n" +
				"        },\n" +
				"        method: 'GET',\n" +
				"        body: JSON.stringify(requestEntity),\n" +
				"        headers: {\n" +
				"            'Accept': 'application/json',\n" +
				"            'Content-Type': 'application/json',\n" +
				"        }\n" +
				"    }\n" +
				"}";

		assertEquals(expected, actual.getResult());
		assertTrue(actual.isSuccess());
	}

	@Test
	public void testList() throws IOException {
		//given
		Map<String, Parameter> parameterMap = new HashMap<>();
		parameterMap.put("offset", new Parameter(ParameterType.NUMBER));
		parameterMap.put("limit", new Parameter(ParameterType.NUMBER));
		List list = new List(parameterMap);
		HttpRequestMethod method = list.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		String expected = "export function listUserRequest(\n" +
				"   offset: number,\n" +
				"   limit: number\n" +
				") {\n" +
				"    return {\n" +
				"        relativeUrl: `/users`,\n" +
				"        parameters: {\n" +
				"            offset: offset,\n" +
				"            limit: limit\n" +
				"        },\n" +
				"        method: 'GET',\n" +
				"        body: null,\n" +
				"        headers: {\n" +
				"            'Accept': 'application/json',\n" +
				"        }\n" +
				"    }\n" +
				"}";

		assertEquals(expected, actual.getResult());
		assertTrue(actual.isSuccess());
	}

	@Test
	public void testGeneralMethodNoSuccess() throws IOException {
		//given
		Method generalMethod = new Method(
				null,
				null,
				null,
				null,
				null,
				Collections.emptyMap(),
				null
		);
		HttpRequestMethod method = generalMethod.buildHttpRequestMethod("/users", "User");

		//when
		HttpRequestMethodGenerator generator = new HttpRequestMethodGenerator(new Freemarker());
		PartialGeneratorResult actual = generator.generate(method);

		//then
		assertFalse(actual.isSuccess());
	}

}
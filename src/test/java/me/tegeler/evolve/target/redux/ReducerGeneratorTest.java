package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ReducerGeneratorTest {
    @Test
    public void test() throws IOException {
        //given
        List<ReducerAction> toggleActions = Collections.singletonList(new ReducerAction("ToggleOpenPopup", "openPopup", "!state.openPopup"));
        Reducer reducer = new Reducer("Collections", "CollectionsAction", "CollectionState", toggleActions);

        //when
        ReducerGenerator reducerGenerator = new ReducerGenerator(new Freemarker());
        PartialGeneratorResult actualResult = reducerGenerator.generate(reducer);

        //then
        String expectedResult = "" +
                "export const reducerCollections: Reducer<CollectionState> =\n" +
                "    (\n" +
                "        state: CollectionState,\n" +
                "        action: CollectionsAction\n" +
                "    ): CollectionState => {\n" +
                "\n" +
                "        switch (action.action) {\n" +
                "            case 'ToggleOpenPopup':\n" +
                "                return {\n" +
                "                    ...state,\n" +
                "                    openPopup: !state.openPopup\n" +
                "                }\n" +
                "        }\n" +
                "\n" +
                "    }";
        assertEquals(expectedResult, actualResult.getResult());
        assertTrue(actualResult.isSuccess());

    }

}
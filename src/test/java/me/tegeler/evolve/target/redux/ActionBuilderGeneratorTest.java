package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ActionBuilderGeneratorTest {
    @Test
    public void test() throws IOException {
        //given
        ActionBuilder actionBuilder = new ActionBuilder("Name", "Action", "Type", "Page", "NameAction");

        //when
        ActionBuilderGenerator actionBuilderGenerator = new ActionBuilderGenerator(new Freemarker());
        PartialGeneratorResult actualResult = actionBuilderGenerator.generate(actionBuilder);

        //then
        String expectedResult = "" +
                "export function buildName():\n" +
                "    NameAction {\n" +
                "    return {\n" +
                "        action: 'Action',\n" +
                "        page: 'Page',\n" +
                "        type: 'Type',\n" +
                "    }\n" +
                "}";
        assertEquals(expectedResult, actualResult.getResult());
        assertTrue(actualResult.isSuccess());

    }

}
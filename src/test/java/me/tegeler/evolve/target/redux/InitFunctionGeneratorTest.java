package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.target.typescript.VariableValue;
import me.tegeler.evolve.util.Freemarker;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class InitFunctionGeneratorTest {

	@Test
	public void test() throws IOException {
		//given
		List<VariableValue> variableValueList = new ArrayList<>();
		variableValueList.add(new VariableValue("openSideMenuRight", "false"));
		variableValueList.add(new VariableValue("openSideMenuLeft", "true"));
		InitFunction initFunction = new InitFunction("NavBar", variableValueList);

		//when
		InitFunctionGenerator initFunctionGenerator = new InitFunctionGenerator(new Freemarker());
		PartialGeneratorResult actualResult = initFunctionGenerator.generate(initFunction);

		//then
		String expectedResult = "export function initNavBarState() {\n" +
				"    return {\n" +
				"        openSideMenuLeft: true,\n" +
				"        openSideMenuRight: false\n" +
				"    }\n" +
				"}";
		assertEquals(expectedResult, actualResult.getResult());
		assertTrue(actualResult.isSuccess());

	}
}
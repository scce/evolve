package me.tegeler.evolve;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public abstract class AbstractResourceBasedTest {

    private final File resourcesDirectory = new File("src/test/resources");

    protected String getTestResourceFileContent(String relativePath) throws IOException {
        return new String(
                Files.readAllBytes(
                        getTestResourceFile(relativePath).toPath()
                )
        );
    }

    protected File getTestResourceFile(String relativePath) {
        return new File(resourcesDirectory, relativePath);
    }
}

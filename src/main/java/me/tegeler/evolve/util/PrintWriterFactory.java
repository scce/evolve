package me.tegeler.evolve.util;

import com.google.inject.Singleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

@Singleton
class PrintWriterFactory {

	PrintWriter buildPrintWriter(File file) throws FileNotFoundException {
		return new PrintWriter(file);
	}
}

package me.tegeler.evolve.util;

import java.util.Arrays;

public class StringHelper {
    public static String encloseWithTicks(String value) {
        return String.format("'%s'", value);
    }
    public static String firstToUpperCase(String name) {
        String head = getHead(name);
        String tail = getTail(name);
        return head.toUpperCase() + tail;
    }

    public static String firstToLowerCase(String name) {
        String head = getHead(name);
        String tail = getTail(name);
        return head.toLowerCase() + tail;
    }

    private static String getTail(String name) {
        return name.substring(1);
    }

    private static String getHead(String name) {
        return name.substring(0, 1);
    }

    public static String concat(String s1, String s2) {
        return s1.concat("\n\n").concat(s2);
    }

    public static String concat(String... strings) {
        return Arrays
                .stream(strings)
                .filter(s -> !s.equals(""))
                .reduce(StringHelper::concat)
                .orElse("Nothing to concat!");
    }
}

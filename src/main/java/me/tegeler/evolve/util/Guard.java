package me.tegeler.evolve.util;

import com.google.inject.Singleton;
import me.tegeler.evolve.exceptions.WorkingDirectoryNotEnabledException;

import java.io.File;

@Singleton
public class Guard {
    public void checkIfWorkingDirectoryIsEnabled(String dir) {
        File evolveFile = new File(dir, FileNameLibrary.EVOLVE_FILE);
        if (!evolveFile.exists()) {
            throw new WorkingDirectoryNotEnabledException(evolveFile.getAbsolutePath());
        }
    }
}

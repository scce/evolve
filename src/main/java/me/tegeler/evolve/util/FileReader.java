package me.tegeler.evolve.util;

import com.google.inject.Singleton;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Singleton
public class FileReader {

    public byte[] read(Path path) throws IOException {
        return Files.readAllBytes(path);
    }
}

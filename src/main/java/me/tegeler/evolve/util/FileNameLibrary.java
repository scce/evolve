package me.tegeler.evolve.util;

import me.tegeler.evolve.commands.GeneratorTask;
import me.tegeler.evolve.target.typescript.Language;

public class FileNameLibrary {
	static final String EVOLVE_FILE = "evolve.json";

	public static String getApiGeneratedFileNameEnding() {
		return getGeneratedFileNameEnding(GeneratorTask.api);
	}

	public static String getEntityGeneratedFileNameEnding() {
		return getGeneratedFileNameEnding(GeneratorTask.entity);
	}

	public static String getFormGeneratedFileNameEnding() {
		return getGeneratedFileNameEnding(GeneratorTask.form);
	}

	public static String getReduxGeneratedFileNameEnding() {
		return getGeneratedFileNameEnding(GeneratorTask.redux);
	}

	public static String getApiModelFileNameEnding() {
		return getModelFileNameEnding(GeneratorTask.api);
	}

	public static String getEntityModelFileNameEnding() {
		return getModelFileNameEnding(GeneratorTask.entity);
	}

	public static String getFormModelFileNameEnding() {
		return getModelFileNameEnding(GeneratorTask.form);
	}

	public static String getReduxModelFileNameEnding() {
		return getModelFileNameEnding(GeneratorTask.redux);
	}

	private static String getModelFileNameEnding(GeneratorTask type) {
		return String.format(".%s.json", type);
	}

	private static String getGeneratedFileNameEnding(GeneratorTask type) {
		return String.format(".%s.evolve.%s", type, Language.FILE_EXTENSION);
	}
}

package me.tegeler.evolve.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Processor;
import me.tegeler.evolve.commands.generate.Result;
import me.tegeler.evolve.commands.generate.api.ApiProcessor;
import me.tegeler.evolve.commands.generate.entity.EntityProcessor;
import me.tegeler.evolve.commands.generate.form.FormProcessor;
import me.tegeler.evolve.commands.generate.redux.ReduxProcessor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Singleton
public class Generator {
	private final Processor entityProcessor;
	private final Processor formProcessor;
	private final Processor apiProcessor;
	private final Processor reduxProcessor;

	@Inject
	public Generator(
			ApiProcessor apiProcessor,
			EntityProcessor entityProcessor,
			FormProcessor formProcessor,
			ReduxProcessor reduxProcessor
	) {
		this.apiProcessor = apiProcessor;
		this.entityProcessor = entityProcessor;
		this.formProcessor = formProcessor;
		this.reduxProcessor = reduxProcessor;
	}

	public void generateApiFiles(String dir) throws IOException {
		Files
				.walk(Paths.get(dir))
				.filter(Files::isRegularFile)
				.filter(path -> path.getFileName().toString().endsWith(FileNameLibrary.getApiModelFileNameEnding()))
				.map(apiProcessor::process)
				.map(Result::getMessage)
				.forEach(System.out::println)
		;
	}

	public void generateEntityFiles(String dir) throws IOException {
		Files
				.walk(Paths.get(dir))
				.filter(Files::isRegularFile)
				.filter(path -> path.getFileName().toString().endsWith(FileNameLibrary.getEntityModelFileNameEnding()))
				.map(entityProcessor::process)
				.map(Result::getMessage)
				.forEach(System.out::println)
		;
	}

	public void generateFormFiles(String dir) throws IOException {
		Files
				.walk(Paths.get(dir))
				.filter(Files::isRegularFile)
				.filter(path -> path.getFileName().toString().endsWith(FileNameLibrary.getFormModelFileNameEnding()))
				.map(formProcessor::process)
				.map(Result::getMessage)
				.forEach(System.out::println)
		;
	}

	public void generateReduxFiles(String dir) throws IOException {
		Files
				.walk(Paths.get(dir))
				.filter(Files::isRegularFile)
				.filter(path -> path.getFileName().toString().endsWith(FileNameLibrary.getReduxModelFileNameEnding()))
				.map(reduxProcessor::process)
				.map(Result::getMessage)
				.forEach(System.out::println)
		;
	}
}

package me.tegeler.evolve.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import java.io.IOException;

@Singleton
public class Freemarker {
	private final Configuration cfg;

	@Inject
	public Freemarker() {
		cfg = new Configuration(Configuration.VERSION_2_3_23);
		cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.DEBUG_HANDLER);
		cfg.setLogTemplateExceptions(false);
	}

	public Template getTemplate(String template) throws IOException {
		return cfg.getTemplate(template);
	}
}

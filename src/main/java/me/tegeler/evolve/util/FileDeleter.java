package me.tegeler.evolve.util;

import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Result;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Singleton
public class FileDeleter {

    public void deleteGeneratedApiFiles(String dir) throws IOException {
        deleteFiles(
                getPathStream(dir)
                        .filter(FileDeleter::isGeneratedApiFile)
        );
    }

    public void deleteGeneratedEntityFiles(String dir) throws IOException {
        deleteFiles(
                getPathStream(dir)
                        .filter(FileDeleter::isGeneratedEntityFile)
        );
    }

    public void deleteGeneratedFormFiles(String dir) throws IOException {
        deleteFiles(
                getPathStream(dir)
                        .filter(FileDeleter::isGeneratedFormFile)
        );
    }

    public void deleteGeneratedReduxFiles(String dir) throws IOException {
        deleteFiles(
                getPathStream(dir)
                        .filter(FileDeleter::isGeneratedReduxFile)
        );
    }

    private void deleteFiles(Stream<Path> pathStream) {
        pathStream.map(Path::toFile)
                .map(this::deleteFile)
                .map(Result::getMessage)
                .forEach(System.out::println);
    }

    private Result deleteFile(File file) {
        if (file.delete()) {
            return Result.createSuccessfulDeleteResult(file);
        }
        return Result.createFailedDeleteResult(file);
    }

    private Stream<Path> getPathStream(String dir) throws IOException {
        return Files
                .walk(Paths.get(dir))
                .filter(Files::isRegularFile);
    }

    private static boolean isGeneratedApiFile(Path path) {
        String fileName = getFileName(path);
        return fileName.endsWith(FileNameLibrary.getApiGeneratedFileNameEnding());
    }

    private static boolean isGeneratedEntityFile(Path path) {
        String fileName = getFileName(path);
        return fileName.endsWith(FileNameLibrary.getEntityGeneratedFileNameEnding());
    }

    private static boolean isGeneratedFormFile(Path path) {
        String fileName = getFileName(path);
        return fileName.endsWith(FileNameLibrary.getFormGeneratedFileNameEnding());
    }

    private static boolean isGeneratedReduxFile(Path path) {
        String fileName = getFileName(path);
        return fileName.endsWith(FileNameLibrary.getReduxGeneratedFileNameEnding());
    }

    private static String getFileName(Path path) {
        return path.getFileName().toString();
    }
}


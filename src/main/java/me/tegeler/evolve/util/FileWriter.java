package me.tegeler.evolve.util;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Rendered;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

@Singleton
public class FileWriter {
    private final PrintWriterFactory printWriterFactory;

    @Inject
    public FileWriter(PrintWriterFactory printWriterFactory) {
        this.printWriterFactory = printWriterFactory;
    }

    public void write(File file, Rendered rendered) throws FileNotFoundException {
        PrintWriter out = printWriterFactory.buildPrintWriter(file);
        out.print(rendered.getRendered());
        out.flush();
        out.close();
    }
}

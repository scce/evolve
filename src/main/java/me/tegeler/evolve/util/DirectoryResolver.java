package me.tegeler.evolve.util;

import com.google.inject.Singleton;

@Singleton
public class DirectoryResolver {

    public String resolveWorkingDirectory() {
        return System.getProperty("user.dir");
    }
}

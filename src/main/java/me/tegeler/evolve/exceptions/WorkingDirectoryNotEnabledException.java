package me.tegeler.evolve.exceptions;

public class WorkingDirectoryNotEnabledException extends RuntimeException {

    public WorkingDirectoryNotEnabledException(String s) {
        super(String.format("Can't find %s as the starting point for the working directory.", s));
    }

}

package me.tegeler.evolve;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

class DependencyModule extends AbstractModule {
	@Override
	protected void configure() {
		bind(ObjectMapper.class).in(Singleton.class);
	}
}

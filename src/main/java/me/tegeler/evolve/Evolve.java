package me.tegeler.evolve;

import com.google.inject.Guice;
import com.google.inject.Injector;
import me.tegeler.evolve.commands.Generate;
import picocli.CommandLine;

import java.util.concurrent.Callable;


@CommandLine.Command(name = "evolve", description = "Evolve is a framework to generate TypeScript files.")
public class Evolve implements Callable<Void> {

	public static void main(String[] args) {
		Evolve
				.buildCommandLine()
				.parseWithHandler(new CommandLine.RunAll(), args);
	}

	private static Generate buildGenerate() {
		Injector injector = Guice.createInjector(new DependencyModule());
		return injector.getInstance(Generate.class);
	}

	private static CommandLine buildCommandLine() {
		return new CommandLine(new Evolve())
				.addSubcommand("generate", buildGenerate());
	}

	@Override
	public Void call() {
		return null;
	}

}
package me.tegeler.evolve.target.redux;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class InitFunctionGenerator extends AbstractPartialGenerator {

	@Inject
	public InitFunctionGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("redux/initFunction.ftlh"));
	}

	public PartialGeneratorResult generate(InitFunction initFunction) {
		Map<String, Object> root = new HashMap<>();
		root.put("initFunction", initFunction);
		return super.generate(root);
	}
}

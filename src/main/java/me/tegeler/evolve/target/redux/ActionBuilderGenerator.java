package me.tegeler.evolve.target.redux;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ActionBuilderGenerator extends AbstractPartialGenerator {

	@Inject
	public ActionBuilderGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("redux/actionBuilder.ftlh"));
	}

	public PartialGeneratorResult generate(ActionBuilder actionBuilder) {
		Map<String, Object> root = new HashMap<>();
		root.put("actionBuilder", actionBuilder);
		return super.generate(root);
	}
}

package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.util.StringHelper;

import java.util.List;

public class Reducer {
    private final String name;
    private final String action;
    private final String state;
    private final List<ReducerAction> toggleActions;

    Reducer(
            String name,
            String action,
            String state,
            List<ReducerAction> toggleActions
    ) {
        this.name = name;
        this.action = action;
        this.state = state;
        this.toggleActions = toggleActions;
    }

    public static Reducer createReducer(
            String name,
            List<ReducerAction> toggleActions
    ) {
        return new Reducer(
                StringHelper.firstToUpperCase(name),
                NameCreator.createNameReduxAction(name),
                NameCreator.createNameReduxState(name),
                toggleActions
        );
    }

    public String getName() {
        return name;
    }

    public String getAction() {
        return action;
    }

    public String getState() {
        return state;
    }

    public List<ReducerAction> getToggleActions() {
        return toggleActions;
    }
}

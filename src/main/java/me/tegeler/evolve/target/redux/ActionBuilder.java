package me.tegeler.evolve.target.redux;

public class ActionBuilder {
    private final String name;
    private final String action;
    private final String type;
    private final String page;
    private final String actionName;

    ActionBuilder(
            String name,
            String action,
            String type,
            String page,
            String actionName
    ) {
        this.name = name;
        this.action = action;
        this.type = type;
        this.page = page;
        this.actionName = actionName;
    }

    public String getName() {
        return name;
    }

    public String getAction() {
        return action;
    }

    public String getType() {
        return type;
    }

    public String getPage() {
        return page;
    }

    public String getActionName() {
        return actionName;
    }

    public static ActionBuilder createToggleAction(
			String stateName,
            String storeName,
            String page
    ) {
        return new ActionBuilder(
                NameCreator.createNameReduxToggleAction(stateName, storeName),
                NameCreator.createNameReduxToggle(stateName),
                NameCreator.createNameReduxType(storeName),
                NameCreator.createNamePage(page),
                NameCreator.createNameReduxToggleAction(stateName, storeName)
        );
    }

    public static ActionBuilder createDisableAction(String stateName, String storeName, String page) {
        return new ActionBuilder(
                NameCreator.createNameReduxDisableAction(stateName, storeName),
                NameCreator.createNameReduxDisable(stateName),
                NameCreator.createNameReduxType(storeName),
                NameCreator.createNamePage(page),
                NameCreator.createNameReduxDisableAction(stateName, storeName)
        );
    }
}

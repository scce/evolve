package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.util.StringHelper;

public class NameCreator {
    public static String createNameReduxType(String name) {
        return String.format("%sRedux", StringHelper.firstToUpperCase(name));
    }

    public static String createNameReduxAction(String name) {
        return String.format("%sReduxAction", StringHelper.firstToUpperCase(name));
    }

    public static String createNameReduxState(String name) {
        return String.format("%sState", StringHelper.firstToUpperCase(name));
    }

    public static String createNameReduxToggle(String stateName) {
        return String.format("Toggle%s", StringHelper.firstToUpperCase(stateName));
    }

    public static String createNameReduxDisable(String stateName) {
        return String.format("Disable%s", StringHelper.firstToUpperCase(stateName));
    }

    public static String createNameReduxToggleAction(String stateName, String storeName) {
        return String.format(
                "%s%sToggleAction",
                StringHelper.firstToUpperCase(storeName),
                StringHelper.firstToUpperCase(stateName)
        );
    }
    public static String createNameReduxDisableAction(String stateName, String storeName) {
        return String.format(
                "%s%sDisableAction",
                StringHelper.firstToUpperCase(storeName),
                StringHelper.firstToUpperCase(stateName)
        );
    }

    public static String createNamePage(String page) {
        return StringHelper.firstToUpperCase(page);
    }

}

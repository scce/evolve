package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.target.typescript.VariableValue;
import me.tegeler.evolve.util.StringHelper;

import java.util.Comparator;
import java.util.List;

public class InitFunction {

	private final String name;
	private final List<VariableValue> variableValueList;

	public InitFunction(String name, List<VariableValue> variableValueList) {
		this.name = StringHelper.firstToUpperCase(name);
		this.variableValueList = variableValueList;
		this.variableValueList.sort(Comparator.comparing(VariableValue::getName));
	}

	public String getName() {
		return name;
	}

	public List<VariableValue> getVariableValueList() {
		return variableValueList;
	}
}

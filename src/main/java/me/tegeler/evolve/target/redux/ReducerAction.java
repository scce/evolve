package me.tegeler.evolve.target.redux;

import me.tegeler.evolve.target.typescript.Language;

public class ReducerAction {
    private final String name;
    private final String property;
    private final String value;

    ReducerAction(String name, String property, String value) {
        this.name = name;
        this.property = property;
        this.value = value;
    }

    public static ReducerAction createDisableAction(String name) {
        return new ReducerAction(NameCreator.createNameReduxDisable(name), name, Language.getFalse());
    }

    public static ReducerAction createToggleAction(String name) {
        return new ReducerAction(NameCreator.createNameReduxToggle(name), name, String.format("!state.%s", name));
    }

    public String getName() {
        return name;
    }

    public String getProperty() {
        return property;
    }

    public String getValue() {
        return value;
    }
}

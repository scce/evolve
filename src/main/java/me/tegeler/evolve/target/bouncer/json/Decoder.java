package me.tegeler.evolve.target.bouncer.json;

import me.tegeler.evolve.util.StringHelper;

import java.util.Comparator;
import java.util.List;

public class Decoder {
    private final String nameLowerCase;
    private final String nameUpperCase;
    private final List<DecoderField> decoderFields;

    public Decoder(String entityName, List<DecoderField> decoderFields) {
        this.nameLowerCase = StringHelper.firstToLowerCase(entityName);
        this.nameUpperCase = StringHelper.firstToUpperCase(entityName);
        this.decoderFields = decoderFields;
        this.decoderFields.sort(Comparator.comparing(DecoderField::getName));
    }

    public String getNameLowerCase() {
        return nameLowerCase;
    }

    public String getNameUpperCase() {
        return nameUpperCase;
    }

    public List<DecoderField> getDecoderFields() {
        return decoderFields;
    }
}

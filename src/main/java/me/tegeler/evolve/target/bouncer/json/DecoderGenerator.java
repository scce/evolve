package me.tegeler.evolve.target.bouncer.json;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class DecoderGenerator extends AbstractPartialGenerator {

	@Inject
	public DecoderGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("entity/decoder.ftlh"));
	}

	public PartialGeneratorResult generate(Decoder decoder) {
		Map<String, Object> root = new HashMap<>();
		root.put("decoder", decoder);
		return super.generate(root);
	}
}

package me.tegeler.evolve.target.bouncer.json;

public class DecoderField {
    private final String name;
    private final String origin;
    private final String decoder;

    public DecoderField(String name, String origin, String decoder) {
        this.name = name;
        this.origin = origin;
        this.decoder = decoder;
    }

    public String getName() {
        return name;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDecoder() {
        return decoder;
    }
}

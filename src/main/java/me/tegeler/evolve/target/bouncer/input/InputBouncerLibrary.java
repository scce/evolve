package me.tegeler.evolve.target.bouncer.input;

public class InputBouncerLibrary {
    private static final String functionCallPattern = "%s()";
    private static final String functionCallWithParameterPattern = "%s(%s)";
    private static final String invalidTypePrefix = "'Invalid%s'";
    private static final String validatorSuffix = "Validator";
    public static final String composeFunction = "compose";
    public static final String errorToNullableFunction = "errorToNullable";
    public static final String libraryName = "input-bouncer";
    public static final String nullType = "null";
    public static final String resultInterface = "Result";

    public static String getStringToFixedPointNumberValidator() {
        return String.format("stringToFixedPointNumber%s", validatorSuffix);
    }

    public static String getMandatoryStringValidator() {
        return String.format("mandatoryString%s", validatorSuffix);
    }

    public static String getSucceedValidator() {
        return String.format("succeed%s", validatorSuffix);
    }

    public static String getMandatoryNullableValidator() {
        return String.format("mandatoryNullable%s", validatorSuffix);
    }

    public static String getStringToIntValidator() {
        return String.format("stringToInt%s", validatorSuffix);
    }

    public static String getEmailValidator() {
        return String.format("email%s", validatorSuffix);
    }

    public static String getComposeFunctionCall(String validators) {
        return String.format(functionCallWithParameterPattern, InputBouncerLibrary.composeFunction, validators);
    }

    public static String getStringToFixedPointNumberValidatorCall() {
        return String.format(functionCallWithParameterPattern, getStringToFixedPointNumberValidator(), 2);
    }


    public static String getMandatoryStringValidatorCall() {
        return String.format(functionCallPattern, getMandatoryStringValidator());
    }

    public static String getSucceedValidatorCall() {
        return String.format(functionCallPattern, getSucceedValidator());
    }

    public static String getMandatoryNullableValidatorCall() {
        return String.format(functionCallPattern, getMandatoryNullableValidator());
    }

    public static String getStringToIntValidatorCall() {
        return String.format(functionCallPattern, getStringToIntValidator());
    }

    public static String getEmailValidatorCall() {
        return String.format(functionCallPattern, getEmailValidator());
    }

    public static String getInvalidEmailType() {
        return String.format(invalidTypePrefix, "Email");
    }

    public static String getInvalidFixedPointNumberType() {
        return String.format(invalidTypePrefix, "FixedPointNumber");
    }

    public static String getInvalidIntType() {
        return String.format("'Invalid%s'", "Int");
    }

    public static String getMandatoryType() {
        return String.format("'%s'", "Mandatory");
    }

}

package me.tegeler.evolve.target.bouncer.json;

import java.util.Arrays;
import java.util.List;

public class JsonBouncerLibrary {
	public static final String lazy = "lazy";
    private static final String decoder = "Decoder";
    private static final String field = "field";
    public static final String array = "array";
    public static final String libraryName = "json-bouncer";
    public static final String nullable = "nullable";
    public static final String object = "object";
    public static final String optional = "optional";


	public static List<String> getStandardImports() {
        return Arrays.asList(decoder, field);
    }

}

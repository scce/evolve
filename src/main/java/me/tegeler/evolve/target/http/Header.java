package me.tegeler.evolve.target.http;

public class Header {
    private final String name;
    private final String value;

    private Header(String name) {
        this.name = name;
        this.value = "application/json";
    }

    public static Header getAcceptHeader() {
        return new Header("Accept");
    }

    public static Header getContentTypeHeader() {
        return new Header("Content-Type");
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

}

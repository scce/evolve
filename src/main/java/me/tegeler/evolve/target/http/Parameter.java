package me.tegeler.evolve.target.http;

import java.util.Map;

public class Parameter {
	private final String name;
	private final String type;

	public Parameter(Map.Entry<String, me.tegeler.evolve.commands.generate.api.model.Parameter> entry) {
		this.name = entry.getKey();
		this.type = entry.getValue().getType().toString();
	}

	private Parameter(String name, String type) {
		this.name = name;
		this.type = type;
	}

	static Parameter getIdParameter() {
		return new Parameter("id", "number");
	}

	public static Parameter getRequestEntity(String requestEntity) {
		return new Parameter("requestEntity", requestEntity);
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}
}

package me.tegeler.evolve.target.http;

import me.tegeler.evolve.commands.generate.api.model.*;
import me.tegeler.evolve.util.StringHelper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class HttpRequestMethod {
    private final String name;
    private final String url;
    private final MethodEnum verb;
    private final List<Parameter> parameters;
    private final List<Header> headers;
    private final String body;

    private HttpRequestMethod(
            String name,
            String url,
            MethodEnum verb,
            List<Parameter> parameters,
            List<Header> headers,
            String body) {
        this.name = name;
        this.url = url;
        this.verb = verb;
        this.parameters = parameters;
        this.headers = headers;
        this.body = body;
    }

    public static HttpRequestMethod buildCreate(String name, String url, Create create) {
        return new HttpRequestMethod(
                createName("create", name),
                url,
                MethodEnum.POST,
                Collections.singletonList(
                        Parameter.getRequestEntity(create.getRequestEntity())
                ),
                Arrays.asList(Header.getAcceptHeader(), Header.getContentTypeHeader()),
                "requestEntity"
        );
    }

    public static HttpRequestMethod buildRetrieve(String name, String url) {
        return new HttpRequestMethod(
                createName("retrieve", name),
                url + "/{id}",
                MethodEnum.GET,
                Collections.singletonList(
                        Parameter.getIdParameter()
                ),
                Collections.singletonList(Header.getAcceptHeader()),
                null
        );
    }

    public static HttpRequestMethod buildUpdate(String name, String url, Update update) {
        return new HttpRequestMethod(
                createName("update", name),
                url + "/{id}",
                MethodEnum.PUT,
                Arrays.asList(
                        Parameter.getIdParameter(),
                        Parameter.getRequestEntity(update.getRequestEntity())
                ),
                Arrays.asList(Header.getAcceptHeader(), Header.getContentTypeHeader()),
                "requestEntity"
        );
    }

    public static HttpRequestMethod buildDelete(String name, String url) {
        return new HttpRequestMethod(
                createName("delete", name),
                url + "/{id}",
                MethodEnum.DELETE,
                Collections.singletonList(Parameter.getIdParameter()),
                Collections.emptyList(),
                null
        );
    }

    public static HttpRequestMethod buildList(String name, String url, me.tegeler.evolve.commands.generate.api.model.List list) {
        return new HttpRequestMethod(
                createName("list", name),
                url,
                MethodEnum.GET,
                list.getParameters(),
                Collections.singletonList(Header.getAcceptHeader()),
                null
        );
    }

    public static HttpRequestMethod build(String url, Method method) {
        return new HttpRequestMethod(
                method.getName(),
                url + "/" + method.getPath(),
                method.getMethod(),
                method.getParameters(),
                method.getHeaders(),
                method.getRequestEntity()
        );
    }

    private static String createName(String prefix, String name) {
        return prefix + StringHelper.firstToUpperCase(name);
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public MethodEnum getVerb() {
        return verb;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public List<Header> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }
}

package me.tegeler.evolve.target.http;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class HttpRequestMethodGenerator extends AbstractPartialGenerator {

	@Inject
	public HttpRequestMethodGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("api/method.ftlh"));
	}

	public PartialGeneratorResult generate(HttpRequestMethod method) {
		Map<String, Object> root = new HashMap<>();
		root.put("method", method);
		return super.generate(root);
	}
}

package me.tegeler.evolve.target.typescript;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class InterfaceGenerator extends AbstractPartialGenerator {

    @Inject
    public InterfaceGenerator(Freemarker freemarker) throws IOException {
        super(freemarker.getTemplate("interface.ftlh"));
    }

    public PartialGeneratorResult generate(Interface anInterface) {
        Map<String, Object> root = new HashMap<>();
        root.put("interface", anInterface);
        return super.generate(root);
    }
}

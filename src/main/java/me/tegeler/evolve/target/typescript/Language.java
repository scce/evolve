package me.tegeler.evolve.target.typescript;

public class Language {

	public static final String FILE_EXTENSION = "ts";

	public static String unionWithNull(String type) {
		return String.format("%s | null", type);
	}

	public static String getString() {
		return Type.STRING.toString().toLowerCase();
	}

	public static String getNumber() {
		return Type.NUMBER.toString().toLowerCase();
	}

	public static String getBoolean() {
		return Type.BOOLEAN.toString().toLowerCase();
	}

	public static String getBooleanValue(boolean value) {
		if (value) {
			return getTrue();
		}
		return getFalse();
	}

	public static String getFalse() {
		return Boolean.FALSE.toString().toLowerCase();
	}

	public static String getTrue() {
		return Boolean.TRUE.toString().toLowerCase();
	}

}

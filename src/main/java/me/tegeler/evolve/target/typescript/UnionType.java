package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.target.redux.NameCreator;
import me.tegeler.evolve.util.StringHelper;

import java.util.List;
import java.util.stream.Collectors;

public class UnionType {
    private final String name;
    private final List<String> subTypes;

    UnionType(String name, List<String> subTypes) {
        this.name = name;
        this.subTypes = subTypes;
    }

    public static UnionType createUnionTypeForForm(String name, List<String> subTypes) {
        String nameUpperCase = StringHelper.firstToUpperCase(name);
        return new UnionType(
                String.format("%sFormAction", nameUpperCase),
                subTypes
                        .stream()
                        .map(s -> String.format("%sFormChange%sAction", nameUpperCase, StringHelper.firstToUpperCase(s)))
                        .sorted()
                        .collect(Collectors.toList())
        );
    }

    public static UnionType createUnionTypeForRedux(String name, List<String> subTypes) {
        return new UnionType(
                NameCreator.createNameReduxAction(name),
                subTypes
        );
    }

    public String getName() {
        return name;
    }

    public List<String> getSubTypes() {
        return subTypes;
    }


}

package me.tegeler.evolve.target.typescript;

import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

public class AbstractPartialGenerator {
	private final Template temp;

	protected AbstractPartialGenerator(Template temp) {
		this.temp = temp;
	}

	protected PartialGeneratorResult generate(Map<String, Object> root) {
		Writer out = new StringWriter();
		try {
			temp.process(root, out);
			return new PartialGeneratorResult(true, out.toString());
		} catch (TemplateException | IOException e) {
			//todo add exception
			return new PartialGeneratorResult(false, null);
		}
	}
}

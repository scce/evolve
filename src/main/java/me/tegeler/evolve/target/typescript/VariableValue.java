package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.commands.generate.redux.model.State;

import java.util.Map;

public class VariableValue {
	private final String name;
	private final String value;

	public VariableValue(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public static VariableValue createFromReduxStateEntry(Map.Entry<String, State> entry) {
		return new VariableValue(entry.getKey(), entry.getValue().getDefault());
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}
}

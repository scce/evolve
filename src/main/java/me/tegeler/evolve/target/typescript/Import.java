package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.bouncer.json.JsonBouncerLibrary;
import me.tegeler.evolve.target.joda.JodaLibrary;
import me.tegeler.evolve.util.StringHelper;

import java.util.*;

public class Import {
	private final TreeSet<String> components;
	private final String library;

	public static Import createForEntity(String name) {
		String firstToUpperCaseName = StringHelper.firstToUpperCase(name);
		String firstToLowerCaseName = StringHelper.firstToLowerCase(name);
		String decoderName = String.format("%sDecoder", firstToLowerCaseName);
		return new Import(
				new TreeSet<>(Arrays.asList(firstToUpperCaseName, decoderName)),
				String.format("./%s.entity.evolve", firstToUpperCaseName)
		);
	}

	public static Import createImportForJsonBouncer(Set<String> imports) {
		imports.addAll(JsonBouncerLibrary.getStandardImports());
		return new Import(imports, JsonBouncerLibrary.libraryName);
	}

	public static Import createImportForBouncer(Set<String> imports) {
		return new Import(imports, InputBouncerLibrary.libraryName);
	}

	public static Import createImportForJoda(Set<String> components) {
		return new Import(components, JodaLibrary.libraryName);
	}

	public static Import createImportFromEntity(String entity) {
		return new Import(
				Collections.singleton(entity),
				String.format("../entity/%s.entity.evolve", entity)
		);
	}

	public static Import createImportForReducer() {
		return new Import(
				Collections.singleton("Reducer"),
				"redux"
		);
	}

	public TreeSet<String> getComponents() {
		return components;
	}

	public String getLibrary() {
		return library;
	}

	@Override
	public boolean equals(Object possibleImport) {
		if (possibleImport.getClass().equals(this.getClass())) {
			Import castImport = (Import) possibleImport;
			return castImport.library.equals(this.library) && castImport.components.equals(this.components);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return library.hashCode() + components.hashCode();
	}

	Import(Set<String> components, String library) {
		this.components = sortComponents(components);
		this.library = library;
	}

	private static TreeSet<String> sortComponents(Set<String> components) {
		List<String> tempComponents = new ArrayList<>(components);
		tempComponents.sort(String::compareTo);
		return new TreeSet<>(tempComponents);
	}

}

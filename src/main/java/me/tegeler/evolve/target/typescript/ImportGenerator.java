package me.tegeler.evolve.target.typescript;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ImportGenerator extends AbstractPartialGenerator {

    @Inject
    public ImportGenerator(Freemarker freemarker) throws IOException {
        super(freemarker.getTemplate("import.ftlh"));
    }

    public PartialGeneratorResult generate(Import anImport) {
        Map<String, Object> root = new HashMap<>();
        root.put("import", anImport);
        return super.generate(root);
    }
}

package me.tegeler.evolve.target.typescript;

public enum Type {
    BOOLEAN,
    NUMBER,
    STRING
}

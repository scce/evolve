package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.form.model.Form;
import me.tegeler.evolve.commands.generate.form.model.Input;
import me.tegeler.evolve.commands.generate.redux.model.State;
import me.tegeler.evolve.target.redux.NameCreator;
import me.tegeler.evolve.util.StringHelper;

import java.util.Map;

public class Attribute {
	private final String name;
	private final String type;

	public Attribute(String name, String type) {
		this.name = name;
		this.type = type;
	}

	static Attribute createReduxToggleAttribute(String stateName) {
		return new Attribute("action", StringHelper.encloseWithTicks(NameCreator.createNameReduxToggle(stateName)));
	}

	static Attribute createReduxDisableAttribute(String stateName) {
		return new Attribute("action", StringHelper.encloseWithTicks(NameCreator.createNameReduxDisable(stateName)));
	}

	static Attribute createReduxTypeAttribute(String storeName) {
		return new Attribute("type", StringHelper.encloseWithTicks(NameCreator.createNameReduxType(storeName)));
	}

	static Attribute createPageAttribute(String page) {
		return new Attribute("page", StringHelper.encloseWithTicks(NameCreator.createNamePage(page)));
	}

	public static Attribute createFormPageAttribute(Deserialized<Form> deserialized) {
		String page = deserialized.getPayload().getPage();
		return createPageAttribute(page);
	}

	public static Attribute createFormTypeAttribute(String formName) {
		return new Attribute("type", StringHelper.encloseWithTicks(String.format("%sForm", StringHelper.firstToUpperCase(formName))));
	}

	public static Attribute createFormActionAttribute(String inputName) {
		return new Attribute("action", StringHelper.encloseWithTicks(String.format("Change%s", inputName)));
	}

	public static Attribute createFormInputAttribute(Map.Entry<String, Input> stringInputEntry) {
		return new Attribute(stringInputEntry.getKey(), stringInputEntry.getValue().getInputType());
	}

	public static Attribute createReduxStateAttribute(Map.Entry<String, State> entry) {
		return new Attribute(entry.getKey(), entry.getValue().getType());
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

}

package me.tegeler.evolve.target.typescript;

public class PartialGeneratorResult {
    private final boolean success;
    private final String result;

    public PartialGeneratorResult(boolean success, String result) {
        this.success = success;
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public boolean isSuccess() {
        return success;
    }
}

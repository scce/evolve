package me.tegeler.evolve.target.typescript;

import me.tegeler.evolve.target.redux.NameCreator;
import me.tegeler.evolve.util.StringHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Interface {
    private final String name;
    private final List<Attribute> attributes;
    private final boolean export;

    public Interface(String name, List<Attribute> attributes, boolean export) {
        this.name = StringHelper.firstToUpperCase(name);
        this.attributes = attributes;
        this.export = export;
        this.attributes.sort(Comparator.comparing(Attribute::getName));
    }

    public static Interface createReduxStateInterface(String name, List<Attribute> attributes) {
        return new Interface(
                NameCreator.createNameReduxState(name),
                attributes,
                true
        );
    }

    public static Interface createFormStateInterface(String name) {
        name = StringHelper.firstToUpperCase(name);
        List<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("input", String.format("%sFormInput", name)));
        attributes.add(new Attribute("submissionAttempted", Language.getBoolean()));
        attributes.add(new Attribute("validation", String.format("%sFormValidation", name)));
        return new Interface(
                String.format("%sFormState", name),
                attributes,
                true
        );
    }

    public static Interface createFormInputInterface(String name, List<Attribute> attributes) {
        name = StringHelper.firstToUpperCase(name);
        return new Interface(
                String.format("%sFormInput", name),
                attributes,
                true
        );
    }

    public static Interface createFormValidationInterface(String name, List<Attribute> attributes) {
        name = StringHelper.firstToUpperCase(name);
        return new Interface(
                String.format("%sFormValidation", name),
                attributes,
                true
        );
    }

    public static Interface createFormDataInterface(String name, List<Attribute> attributes) {
        name = StringHelper.firstToUpperCase(name);
        return new Interface(
                String.format("%sFormData", name),
                attributes,
                true
        );
    }

    public static Interface createFormChangeActionInterface(
            String name,
            String inputName,
            List<Attribute> attributes
    ) {
        name = StringHelper.firstToUpperCase(name);
        return new Interface(
                String.format("%sFormChange%sAction", name, inputName),
                attributes,
                false
        );
    }

    public static Interface createReduxToggleActionInterface(
            String stateName, String storeName, String page
    ) {
        return new Interface(
                NameCreator.createNameReduxToggleAction(stateName, storeName),
                Arrays.asList(
                        Attribute.createPageAttribute(page),
                        Attribute.createReduxToggleAttribute(stateName),
                        Attribute.createReduxTypeAttribute(storeName)
                ),
                false
        );
    }

    public static Interface createReduxDisableActionInterface(String stateName, String storeName, String page) {
        return new Interface(
                NameCreator.createNameReduxDisableAction(stateName, storeName),
                Arrays.asList(
                        Attribute.createPageAttribute(page),
                        Attribute.createReduxDisableAttribute(stateName),
                        Attribute.createReduxTypeAttribute(storeName)
                ),
                false
        );
    }

    public String getName() {
        return name;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public boolean isExport() {
        return export;
    }

}


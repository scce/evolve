package me.tegeler.evolve.target.typescript;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class UnionTypeGenerator extends AbstractPartialGenerator {

	@Inject
	public UnionTypeGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("unionType.ftlh"));
	}

	public PartialGeneratorResult generate(UnionType unionType) {
		Map<String, Object> root = new HashMap<>();
		root.put("unionType", unionType);
		return super.generate(root);
	}
}

package me.tegeler.evolve.target.joda;

public class JodaLibrary {
    public static final String libraryName = "js-joda";
    public static final String localDateType = "LocalDate";
    public static final String localDateTimeType = "LocalDateTime";
}

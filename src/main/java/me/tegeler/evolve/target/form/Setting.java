package me.tegeler.evolve.target.form;

public class Setting {

	private final String name;
	private final String mandatory;

	public Setting(String name, String mandatory) {
		this.name = name;
		this.mandatory = mandatory;
	}

	public String getName() {
		return name;
	}

	public String getMandatory() {
		return mandatory;
	}
}

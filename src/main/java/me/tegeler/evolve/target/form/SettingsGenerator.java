package me.tegeler.evolve.target.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class SettingsGenerator extends AbstractPartialGenerator {

	@Inject
	public SettingsGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("form/settings.ftlh"));
	}

	public PartialGeneratorResult generate(Settings settings) {
		Map<String, Object> root = new HashMap<>();
		root.put("settings", settings);
		return super.generate(root);
	}
}

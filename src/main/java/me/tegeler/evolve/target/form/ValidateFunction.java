package me.tegeler.evolve.target.form;

import java.util.List;

public class ValidateFunction {
	private final String name;
	private final String inputParameter;
	private final String returnValidation;
	private final String returnData;
	private final List<ValidateItem> validateItemList;

	public ValidateFunction(
			String name,
			String inputParameter,
			String returnValidation,
			String returnData,
			List<ValidateItem> validateItemList
	) {
		this.name = name;
		this.inputParameter = inputParameter;
		this.returnValidation = returnValidation;
		this.returnData = returnData;
		this.validateItemList = validateItemList;
	}

	public String getName() {
		return name;
	}

	public String getInputParameter() {
		return inputParameter;
	}

	public String getReturnValidation() {
		return returnValidation;
	}

	public String getReturnData() {
		return returnData;
	}

	public List<ValidateItem> getValidateItemList() {
		return validateItemList;
	}
}

package me.tegeler.evolve.target.form;

public class ChangeFunction {
	private final String action;
	private final String builderFunctionName;
	private final String changeValue;
	private final String inputType;
	private final String page;
	private final String actionType;
	private final String type;

	public ChangeFunction(
			String action,
			String builderFunctionName,
			String changeValue,
			String inputType,
			String page,
			String actionType,
			String type
	) {
		this.action = action;
		this.builderFunctionName = builderFunctionName;
		this.changeValue = changeValue;
		this.inputType = inputType;
		this.page = page;
		this.actionType = actionType;
		this.type = type;
	}

	public String getAction() {
		return action;
	}

	public String getBuilderFunctionName() {
		return builderFunctionName;
	}

	public String getChangeValue() {
		return changeValue;
	}

	public String getPage() {
		return page;
	}

	public String getActionType() {
		return actionType;
	}

	public String getType() {
		return type;
	}

	public String getInputType() {
		return inputType;
	}
}

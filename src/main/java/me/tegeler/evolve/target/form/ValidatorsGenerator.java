package me.tegeler.evolve.target.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ValidatorsGenerator extends AbstractPartialGenerator {

	@Inject
	public ValidatorsGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("form/validators.ftlh"));
	}

	public PartialGeneratorResult generate(Validators validators) {
		Map<String, Object> root = new HashMap<>();
		root.put("validators", validators);
		return super.generate(root);
	}
}

package me.tegeler.evolve.target.form;

public class Validator {
    private final String name;
    private final String validator;

    public Validator(String name, String validator) {
        this.name = name;
        this.validator = validator;
    }

    public String getName() {
        return name;
    }

    public String getValidator() {
        return validator;
    }
}

package me.tegeler.evolve.target.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ReducerGenerator extends AbstractPartialGenerator {

	@Inject
	public ReducerGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("form/reducer.ftlh"));
	}

	public PartialGeneratorResult generate(Reducer reducer) {
		Map<String, Object> root = new HashMap<>();
		root.put("reducer", reducer);
		return super.generate(root);
	}
}

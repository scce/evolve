package me.tegeler.evolve.target.form;

import java.util.List;

public class Validators {
    private final List<Validator> validators;

    public Validators(List<Validator> validators) {
        this.validators = validators;
    }

    public List<Validator> getValidators() {
        return validators;
    }
}

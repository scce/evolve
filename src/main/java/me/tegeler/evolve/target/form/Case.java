package me.tegeler.evolve.target.form;

public class Case {

	private final String name;
	private final String action;

	public Case(
			String name,
			String action
	) {
		this.name = name;
		this.action = action;
	}

	public String getAction() {
		return action;
	}

	public String getName() {
		return name;
	}
}

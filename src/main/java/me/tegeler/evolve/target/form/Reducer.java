package me.tegeler.evolve.target.form;

import java.util.List;

public class Reducer {
	private final String name;
	private final String stateParameter;
	private final String actionParameter;
	private final String returnType;
	private final List<Case> caseList;

	public Reducer(
			String name,
			String stateParameter,
			String actionParameter,
			String returnType,
			List<Case> caseList
	) {
		this.name = name;
		this.stateParameter = stateParameter;
		this.actionParameter = actionParameter;
		this.returnType = returnType;
		this.caseList = caseList;
	}

	public String getName() {
		return name;
	}

	public String getStateParameter() {
		return stateParameter;
	}

	public String getActionParameter() {
		return actionParameter;
	}

	public String getReturnType() {
		return returnType;
	}

	public List<Case> getCaseList() {
		return caseList;
	}
}

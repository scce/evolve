package me.tegeler.evolve.target.form;

import me.tegeler.evolve.util.StringHelper;

public class NameCreator {

	public static String createReducerName(String name) {
		return String.format("%sFormReducer", StringHelper.firstToLowerCase(name));
	}

	public static String createFormStateName(String name) {
		return String.format("%sFormState", StringHelper.firstToUpperCase(name));
	}

	public static String createFormActionName(String name) {
		return String.format("%sFormAction", StringHelper.firstToUpperCase(name));
	}

	public static String createChangeActionName(String name) {
		return String.format("Change%s", StringHelper.firstToUpperCase(name));
	}

	public static String createFormName(String name) {
		return String.format("%sForm", StringHelper.firstToUpperCase(name));
	}

	public static String createFormChangeActionName(String name, String key) {
		return String.format("%sFormChange%sAction", StringHelper.firstToUpperCase(name), StringHelper.firstToUpperCase(key));
	}

	public static String createBuildMethodName(String name, String key) {
		return String.format("build%s", createFormChangeActionName(name, key));
	}

	public static String createChangeValueName(String name) {
		return StringHelper.firstToLowerCase(name);
	}

	public static String createSettingsName(String name) {
		return String.format("%sFormSettings", StringHelper.firstToLowerCase(name));
	}

	public static String createValidateFunctionName(String name) {
		return String.format("validate%sForm", StringHelper.firstToUpperCase(name));
	}

	public static String createValidateFunctionInputName(String name) {
		return String.format("%sFormInput", StringHelper.firstToUpperCase(name));
	}

	public static String createValidateFunctionReturnValidationName(String name) {
		return String.format("%sFormValidation", StringHelper.firstToUpperCase(name));
	}

	public static String createValidateFunctionReturnDataName(String name) {
		return String.format("%sFormData", StringHelper.firstToUpperCase(name));
	}
}

package me.tegeler.evolve.target.form;

public class ValidateItem {
	private final String name;

	public ValidateItem(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}

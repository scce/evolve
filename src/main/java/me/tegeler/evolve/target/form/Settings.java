package me.tegeler.evolve.target.form;

import java.util.List;

public class Settings {
	private final String name;
	private final List<Setting> settingList;

	public Settings(
			String name,
			List<Setting> settingList
	) {
		this.name = name;
		this.settingList = settingList;
	}

	public String getName() {
		return name;
	}

	public List<Setting> getSettingList() {
		return settingList;
	}
}

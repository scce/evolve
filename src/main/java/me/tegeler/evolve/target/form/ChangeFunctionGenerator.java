package me.tegeler.evolve.target.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.target.typescript.AbstractPartialGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.Freemarker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ChangeFunctionGenerator extends AbstractPartialGenerator {

	@Inject
	public ChangeFunctionGenerator(Freemarker freemarker) throws IOException {
		super(freemarker.getTemplate("form/changeFunction.ftlh"));
	}

	public PartialGeneratorResult generate(ChangeFunction changeFunction) {
		Map<String, Object> root = new HashMap<>();
		root.put("changeFunction", changeFunction);
		return super.generate(root);
	}
}

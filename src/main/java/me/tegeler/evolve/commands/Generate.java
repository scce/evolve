package me.tegeler.evolve.commands;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.util.DirectoryResolver;
import me.tegeler.evolve.util.FileDeleter;
import me.tegeler.evolve.util.Generator;
import me.tegeler.evolve.util.Guard;
import picocli.CommandLine;

import java.io.IOException;
import java.util.concurrent.Callable;

@Singleton
@CommandLine.Command(name = "generate", description = "Generates evolve files.")
public class Generate implements Callable<Void> {

	@CommandLine.Option(names = {"-t", "--task"}, description = "Task to be execute.")
	private GeneratorTask generatorTask;

	private final DirectoryResolver resolver;
	private final FileDeleter fileDeleter;
	private final Generator generator;
	private final Guard guard;

	@Inject
	public Generate(
			DirectoryResolver resolver,
			FileDeleter fileDeleter,
			Generator generator,
			Guard guard
	) {
		this.fileDeleter = fileDeleter;
		this.generator = generator;
		this.guard = guard;
		this.resolver = resolver;
	}

	@Override
	public Void call() throws Exception {
		String dir = resolver.resolveWorkingDirectory();
		guard.checkIfWorkingDirectoryIsEnabled(dir);
		if (generatorTask == null) {
			api(dir);
			entity(dir);
			form(dir);
			redux(dir);
		} else if (generatorTask == GeneratorTask.api) {
			api(dir);
		} else if (generatorTask == GeneratorTask.entity) {
			entity(dir);
		} else if (generatorTask == GeneratorTask.form) {
			form(dir);
		} else if (generatorTask == GeneratorTask.redux) {
			redux(dir);
		}
		return null;
	}

	private void api(String dir) throws IOException {
		fileDeleter.deleteGeneratedApiFiles(dir);
		generator.generateApiFiles(dir);
	}

	private void entity(String dir) throws IOException {
		fileDeleter.deleteGeneratedEntityFiles(dir);
		generator.generateEntityFiles(dir);
	}

	private void form(String dir) throws IOException {
		fileDeleter.deleteGeneratedFormFiles(dir);
		generator.generateFormFiles(dir);
	}

	private void redux(String dir) throws IOException {
		fileDeleter.deleteGeneratedReduxFiles(dir);
		generator.generateReduxFiles(dir);
	}

	void setGeneratorTask(GeneratorTask generatorTask) {
		this.generatorTask = generatorTask;
	}
}

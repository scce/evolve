package me.tegeler.evolve.commands.generate;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public abstract class Deserializer<T> {
    protected final ObjectMapper mapper;

    protected Deserializer(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public abstract Deserialized<T> deserialize(Validated read) throws IOException;
}

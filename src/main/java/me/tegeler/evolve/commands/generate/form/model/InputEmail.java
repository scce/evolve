package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.typescript.Language;

import java.util.List;

public class InputEmail extends Input {
    public InputEmail(
            @JsonProperty(value = "default") String defaultValue,
            @JsonProperty(value = "mandatory") boolean mandatory
    ) {
        this.defaultValue = defaultValue;
        this.mandatory = mandatory;
        this.type = InputType.EMAIL;
        this.inputType = Language.getString();
        this.dataType = Language.getString();
        this.validator = buildValidators();
        this.validationType = buildValidationType();
    }

    @Override
    protected void addMandatoryValidator(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryStringValidator());
    }

    @Override
    protected void addTypeSpecificValidator(List<String> validators) {
        validators.add(InputBouncerLibrary.getEmailValidator());
    }

    @Override
    protected void addMandatoryValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryStringValidatorCall());
    }

    @Override
    protected void addTypeSpecificValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getEmailValidatorCall());

    }

    @Override
    protected void addTypeSpecificValidationType(List<String> validationTypes) {
        validationTypes.add(InputBouncerLibrary.getInvalidEmailType());
    }

}

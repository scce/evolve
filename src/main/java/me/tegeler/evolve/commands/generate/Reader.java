package me.tegeler.evolve.commands.generate;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.util.FileReader;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;

@Singleton
public class Reader {
    private final FileReader fileReader;

    @Inject
    public Reader(FileReader fileReader) {
        this.fileReader = fileReader;
    }

    public Read read(Path path) throws IOException {
        byte[] read = fileReader.read(path);
        String json = new String(read, Charset.defaultCharset());
        return new Read(path, json);
    }
}

package me.tegeler.evolve.commands.generate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.IOException;

public abstract class Validator {
    private final JsonSchemaFactory factory;
    private final ObjectMapper mapper;
    private final String type;

    protected Validator(ObjectMapper mapper, String type) {
        this.mapper = mapper;
        this.type = type;
        this.factory = JsonSchemaFactory.byDefault();
    }

    public Validated validate(Read read) throws ProcessingException, IOException {
        ProcessingReport report = actualValidate(read);
        if (!report.isSuccess()) {
            throw new RuntimeException("The template config is not valid");
        }
        return new Validated(read.getPath(), read.getJson());
    }

    private ProcessingReport actualValidate(Read read) throws IOException, ProcessingException {
        JsonNode templateConfigJsonNode = mapper.readTree(read.getJson());
        JsonSchema schema = getJsonSchema();
        return schema.validate(templateConfigJsonNode);
    }

    private JsonSchema getJsonSchema() throws ProcessingException {
        return factory.getJsonSchema(getSchemaUri());
    }

    private String getSchemaUri() {
        return String.format("resource:/%s.schema.json", type);
    }

}

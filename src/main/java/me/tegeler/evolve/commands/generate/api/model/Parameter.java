package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Parameter {
	private final ParameterType type;

	public Parameter(@JsonProperty(value = "type", required = true) ParameterType type) {
		this.type = type;
	}

	public ParameterType getType() {
		return type;
	}
}

package me.tegeler.evolve.commands.generate.redux;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Writer;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.FileWriter;

import java.io.File;

@Singleton
public class ReduxWriter extends Writer {

	@Inject
	public ReduxWriter(FileWriter fileWriter) {
		super(fileWriter);
	}

	@Override
	protected File createTargetFile(Rendered rendered) {
		return actualCreateTargetFile(
				rendered,
				FileNameLibrary.getReduxModelFileNameEnding(),
				FileNameLibrary.getReduxGeneratedFileNameEnding()
		);
	}

}

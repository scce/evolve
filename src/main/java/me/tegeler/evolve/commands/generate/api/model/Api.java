package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class Api {
    private final String path;
    private final Rest rest;
    private final List<Method> methods;

    public Api(
            @JsonProperty(value = "path") String path,
            @JsonProperty(value = "rest") Rest rest,
            @JsonProperty(value = "methods") List<Method> methods
    ) {
        this.path = path;
        this.rest = rest;
        this.methods = methods;
    }

    public Stream<MethodInterface> getRestAndMethods() {
        List<MethodInterface> list = new ArrayList<>();
        if (rest != null) {
            list.add(rest.getCreate());
            list.add(rest.getRetrieve());
            list.add(rest.getUpdate());
            list.add(rest.getDelete());
            list.add(rest.getList());
        }
        list.addAll(methods);
        return list
                .stream()
                .filter(Objects::nonNull);
    }

    public String getPath() {
        return path;
    }

    public List<Method> getMethods() {
        return methods;
    }
}

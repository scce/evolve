package me.tegeler.evolve.commands.generate.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Renderer;
import me.tegeler.evolve.commands.generate.form.model.Form;
import me.tegeler.evolve.commands.generate.form.model.Input;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.form.*;
import me.tegeler.evolve.target.typescript.*;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.StringHelper;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class FormRenderer extends Renderer<Form> {

	private final ImportGenerator importGenerator;
	private final InterfaceGenerator interfaceGenerator;
	private final UnionTypeGenerator unionTypeGenerator;
	private final ChangeFunctionGenerator changeFunctionGenerator;
	private final ReducerGenerator reducerGenerator;
	private final SettingsGenerator settingsGenerator;
	private final ValidatorsGenerator validatorsGenerator;
	private final ValidateFunctionGenerator validateFunctionGenerator;

	@Inject
	public FormRenderer(
			ImportGenerator importGenerator,
			InterfaceGenerator interfaceGenerator,
			UnionTypeGenerator unionTypeGenerator,
			ChangeFunctionGenerator changeFunctionGenerator,
			ReducerGenerator reducerGenerator,
			SettingsGenerator settingsGenerator,
			ValidatorsGenerator validatorsGenerator,
			ValidateFunctionGenerator validateFunctionGenerator
	) {
		super();
		this.importGenerator = importGenerator;
		this.interfaceGenerator = interfaceGenerator;
		this.unionTypeGenerator = unionTypeGenerator;
		this.changeFunctionGenerator = changeFunctionGenerator;
		this.reducerGenerator = reducerGenerator;
		this.settingsGenerator = settingsGenerator;
		this.validatorsGenerator = validatorsGenerator;
		this.validateFunctionGenerator = validateFunctionGenerator;
	}

	public Rendered render(Deserialized<Form> deserialized) {
		String name = getName(deserialized, FileNameLibrary.getFormModelFileNameEnding());
		String imports = renderImportsImports(deserialized.getPayload());
		String interfaces = renderInterfaces(name, deserialized);
		String unionType = renderUnionType(name, deserialized);
		String changeActions = renderChangeActions(name, deserialized);
		String changeFunctions = renderChangeFunctions(name, deserialized);
		String reducer = renderReducer(name, deserialized);
		String settings = renderSettings(name, deserialized);
		String validators = renderValidators(deserialized);
		String validateFunction = renderValidateFunction(name, deserialized);
		return new Rendered(deserialized.getPath(), StringHelper.concat(
				imports,
				interfaces,
				unionType,
				changeActions,
				changeFunctions,
				reducer,
				settings,
				validators,
				validateFunction
		));
	}

	private String renderValidateFunction(String name, Deserialized<Form> deserialized) {
		List<ValidateItem> validateItemList = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(this::createValidateItem)
				.collect(Collectors.toList());

		ValidateFunction validateFunction = new ValidateFunction(
				NameCreator.createValidateFunctionName(name),
				NameCreator.createValidateFunctionInputName(name),
				NameCreator.createValidateFunctionReturnValidationName(name),
				NameCreator.createValidateFunctionReturnDataName(name),
				validateItemList
		);

		return validateFunctionGenerator.generate(validateFunction).getResult();
	}

	private ValidateItem createValidateItem(Map.Entry<String, Input> stringInputEntry) {
		return new ValidateItem(StringHelper.firstToLowerCase(stringInputEntry.getKey()));
	}

	private String renderValidators(Deserialized<Form> deserialized) {
		List<Validator> validatorList = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(this::createValidator)
				.collect(Collectors.toList());
		Validators validators = new Validators(
				validatorList
		);

		return validatorsGenerator.generate(validators).getResult();
	}

	private Validator createValidator(Map.Entry<String, Input> stringInputEntry) {
		return new Validator(
				stringInputEntry.getKey(),
				stringInputEntry.getValue().getValidator()
		);
	}

	private String renderSettings(String name, Deserialized<Form> deserialized) {
		List<Setting> settingList = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(this::createSetting)
				.collect(Collectors.toList());

		Settings settings = new Settings(
				NameCreator.createSettingsName(name),
				settingList
		);

		return settingsGenerator.generate(settings).getResult();
	}

	private Setting createSetting(Map.Entry<String, Input> stringInputEntry) {
		return new Setting(stringInputEntry.getKey(), Language.getBooleanValue(stringInputEntry.getValue().getMandatory()));
	}

	private String renderReducer(String name, Deserialized<Form> deserialized) {
		List<Case> caseList = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(this::createCase)
				.collect(Collectors.toList());

		Reducer reducer = new Reducer(
				NameCreator.createReducerName(name),
				NameCreator.createFormStateName(name),
				NameCreator.createFormActionName(name),
				NameCreator.createFormStateName(name),
				caseList
		);
		return reducerGenerator.generate(reducer).getResult();
	}

	private Case createCase(Map.Entry<String, Input> entry) {
		return new Case(
				entry.getKey(),
				NameCreator.createChangeActionName(entry.getKey())
		);
	}

	private String renderChangeFunctions(String name, Deserialized<Form> deserialized) {
		return deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(entry -> createFormChangeMethod(name, deserialized.getPayload().getPage(), entry))
				.map(changeFunctionGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private ChangeFunction createFormChangeMethod(String name, String page, Map.Entry<String, Input> entry) {
		String actionType = NameCreator.createFormChangeActionName(name, entry.getKey());
		String builderMethodName = NameCreator.createBuildMethodName(name, entry.getKey());
		String action = NameCreator.createChangeActionName(entry.getKey());
		String changeValue = NameCreator.createChangeValueName(entry.getKey());
		String inputType = entry.getValue().getInputType();
		String pageFirstUpperCase = StringHelper.firstToUpperCase(page);
		String type = NameCreator.createFormName(name);
		return new ChangeFunction(
				action,
				builderMethodName,
				changeValue,
				inputType,
				pageFirstUpperCase,
				actionType,
				type
		);
	}

	private String renderChangeActions(String name, Deserialized<Form> deserialized) {
		return deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey))
				.map(stringInputEntry -> createFormChangeInterface(name, deserialized, stringInputEntry))
				.map(interfaceGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private Interface createFormChangeInterface(String formName, Deserialized<Form> deserialized, Map.Entry<String, Input> stringInputEntry) {
		String inputName = StringHelper.firstToUpperCase(stringInputEntry.getKey());
		return Interface.createFormChangeActionInterface(
				formName,
				inputName,
				Arrays.asList(
						Attribute.createFormPageAttribute(deserialized),
						Attribute.createFormTypeAttribute(formName),
						Attribute.createFormActionAttribute(inputName),
						Attribute.createFormInputAttribute(stringInputEntry)
				)
		);
	}

	private String renderInterfaces(String name, Deserialized<Form> deserialized) {
		return Stream.of(
				Interface.createFormStateInterface(name),
				createFormInputInterface(name, deserialized),
				createFormValidationInterface(name, deserialized),
				createFormDataInterface(name, deserialized)
		)
				.map(interfaceGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private Interface createFormDataInterface(String name, Deserialized<Form> deserialized) {
		List<Attribute> attributes = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.map(FormRenderer::getDataInterface)
				.collect(Collectors.toList());
		return Interface.createFormDataInterface(name, attributes);
	}

	private static Attribute getDataInterface(Map.Entry<String, Input> stringInputEntry) {
		return stringInputEntry.getValue().createDataInterface(stringInputEntry.getKey());
	}

	private Interface createFormValidationInterface(String name, Deserialized<Form> deserialized) {
		List<Attribute> attributes = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.map(FormRenderer::getValidationInterface)
				.collect(Collectors.toList());
		return Interface.createFormValidationInterface(name, attributes);
	}

	private static Attribute getValidationInterface(Map.Entry<String, Input> stringInputEntry) {
		return stringInputEntry.getValue().createValidationInterface(stringInputEntry.getKey());
	}

	private Interface createFormInputInterface(String name, Deserialized<Form> deserialized) {
		List<Attribute> attributes = deserialized
				.getPayload()
				.getInputs()
				.entrySet()
				.stream()
				.map(FormRenderer::getInputInterface)
				.collect(Collectors.toList());
		return Interface.createFormInputInterface(name, attributes);
	}

	private static Attribute getInputInterface(Map.Entry<String, Input> stringInputEntry) {
		return stringInputEntry.getValue().createInputInterface(stringInputEntry.getKey());
	}

	private String renderImportsImports(Form formMap) {
		List<Import> imports = new ArrayList<>();
		Import validatorImport = createValidatorImport(formMap);
		Import jsJodaImport = createJsJodaImport(formMap);
		addImportIfImportHasComponents(imports, validatorImport);
		addImportIfImportHasComponents(imports, jsJodaImport);
		return imports
				.stream()
				.map(importGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private void addImportIfImportHasComponents(List<Import> imports, Import anImport) {
		if (anImport.getComponents().size() > 0) {
			imports.add(anImport);
		}
	}

	private Import createValidatorImport(Form formMap) {
		Set<String> imports = formMap
				.getInputs()
				.values()
				.stream()
				.flatMap(Input::getUsedValidators)
				.collect(Collectors.toSet());
		imports.add(InputBouncerLibrary.resultInterface);
		imports.add(InputBouncerLibrary.errorToNullableFunction);
		return Import.createImportForBouncer(imports);
	}

	private Import createJsJodaImport(Form formMap) {
		Set<String> imports = formMap
				.getInputs()
				.values()
				.stream()
				.filter(Input::isDate)
				.map(Input::getDataType)
				.collect(Collectors.toSet());
		return Import.createImportForJoda(imports);
	}

	private String renderUnionType(String name, Deserialized<Form> deserialized) {
		List<String> subTypes = new ArrayList<>(deserialized
				.getPayload()
				.getInputs()
				.keySet());
		return unionTypeGenerator
				.generate(UnionType.createUnionTypeForForm(name, subTypes))
				.getResult();
	}

}

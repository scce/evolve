package me.tegeler.evolve.commands.generate.api.model;

import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.stream.Stream;

public class Delete implements MethodInterface {
    @Override
    public HttpRequestMethod buildHttpRequestMethod(String baseUrl, String name) {
        return HttpRequestMethod.buildDelete(name, baseUrl);
    }

    @Override
    public Stream<String> getEntities() {
        return Stream.empty();
    }
}

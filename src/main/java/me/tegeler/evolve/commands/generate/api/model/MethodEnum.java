package me.tegeler.evolve.commands.generate.api.model;

public enum MethodEnum {
    GET, POST, PUT, DELETE
}

package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.typescript.Language;

import java.util.List;

public class InputBoolean extends Input {
    public InputBoolean(
            @JsonProperty(value = "default") String defaultValue,
            @JsonProperty(value = "mandatory") boolean mandatory
    ) {
        this.defaultValue = defaultValue;
        this.mandatory = mandatory;
        this.type = InputType.BOOLEAN;
        this.inputType = Language.getBoolean();
        this.dataType = Language.getBoolean();
        this.validator = buildValidators();
        this.validationType = buildValidationType();
    }

    @Override
    protected void addMandatoryValidator(List<String> validators) {
        // not needed
    }

    @Override
    protected void addTypeSpecificValidator(List<String> validators) {
        validators.add(InputBouncerLibrary.getSucceedValidator());
    }

    @Override
    protected void addMandatoryValidatorCall(List<String> validators) {
        // not needed
    }

    @Override
    protected void addTypeSpecificValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getSucceedValidatorCall());
    }

    @Override
    protected void addTypeSpecificValidationType(List<String> validationTypes) {
        // not needed
    }
}

package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.Objects;
import java.util.stream.Stream;

public class Retrieve implements MethodInterface {
    private final String responseEntity;

    public Retrieve(
            @JsonProperty(value = "responseEntity", required = true) String responseEntity
    ) {
        this.responseEntity = responseEntity;
    }

    @Override
    public HttpRequestMethod buildHttpRequestMethod(String baseUrl, String name) {
        return HttpRequestMethod.buildRetrieve(name, baseUrl );
    }

    @Override
    public Stream<String> getEntities() {
        return Stream
                .of(responseEntity)
                .filter(Objects::nonNull);
    }
}

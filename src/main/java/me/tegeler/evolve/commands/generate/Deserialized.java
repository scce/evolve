package me.tegeler.evolve.commands.generate;

import java.nio.file.Path;

public class Deserialized<T> {

    private final Path path;
    private final T payload;

    public Deserialized(Path path, T payload) {
        this.path = path;
        this.payload = payload;
    }

    public Path getPath() {
        return path;
    }

    public T getPayload() {
        return payload;
    }
}

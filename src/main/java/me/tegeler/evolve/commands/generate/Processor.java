package me.tegeler.evolve.commands.generate;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.IOException;
import java.nio.file.Path;

public class Processor<T> {

    private final Deserializer<T> deserializer;
    private final Reader reader;
    private final Renderer<T> renderer;
    private final Validator validator;
    private final Writer writer;

    public Processor(
            Deserializer<T> deserializer,
            Reader reader,
            Renderer<T> renderer,
            Validator validator,
            Writer writer
    ) {
        this.deserializer = deserializer;
        this.reader = reader;
        this.renderer = renderer;
        this.validator = validator;
        this.writer = writer;
    }

    public Result process(Path path) {
        try {
            Read read = reader.read(path);
            Validated validated = validator.validate(read);
            Deserialized<T> deserialized = deserializer.deserialize(validated);
            Rendered rendered = renderer.render(deserialized);
            return writer.write(rendered);
        } catch (IOException | ProcessingException e) {
            return Result.createExceptionResult(e);
        }
    }

}

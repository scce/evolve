package me.tegeler.evolve.commands.generate.entity.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EntityBoolean extends Entity {
    public EntityBoolean(
            @JsonProperty(value = "array") boolean array,
            @JsonProperty(value = "null") boolean nullAble,
            @JsonProperty(value = "optional") boolean optional,
            @JsonProperty(value = "origin") String origin
    ) {
        this.array = array;
        this.nullAble = nullAble;
        this.optional = optional;
        this.origin = origin;
        this.type = EntityType.BOOLEAN;
    }
}

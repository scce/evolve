package me.tegeler.evolve.commands.generate.redux.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Redux {

	private final String page;
	private final Map<String, State> states;

	public Redux(
			@JsonProperty(value = "page") String page,
			@JsonProperty(value = "states") Map<String, State> states
	) {
		this.page = page;
		this.states = states;
	}

	public String getPage() {
		return page;
	}

	public Map<String, State> getStates() {
		return states;
	}
}

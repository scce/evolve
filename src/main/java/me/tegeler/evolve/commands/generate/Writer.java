package me.tegeler.evolve.commands.generate;

import me.tegeler.evolve.util.FileWriter;

import java.io.File;
import java.io.FileNotFoundException;

public abstract class Writer {

    private final FileWriter fileWriter;

    protected Writer(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
    }

    public Result write(Rendered rendered) throws FileNotFoundException {
        File file = createTargetFile(rendered);
        fileWriter.write(file, rendered);
        return Result.createSuccessfulRenderedResult(rendered);
    }

    protected abstract File createTargetFile(Rendered rendered);

    protected File actualCreateTargetFile(
            Rendered rendered,
            String reduxModelFileNameEnding,
            String reduxGeneratedFileNameEnding
    ) {
        String sanitizedFilename = rendered.getPath().toString().replace(reduxModelFileNameEnding, "");
        return new File(sanitizedFilename + reduxGeneratedFileNameEnding);
    }
}

package me.tegeler.evolve.commands.generate.api.model;

import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.stream.Stream;

public interface MethodInterface {
    HttpRequestMethod buildHttpRequestMethod(
            String baseUrl,
            String name
    );

    Stream<String> getEntities();
}

package me.tegeler.evolve.commands.generate.entity;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Processor;
import me.tegeler.evolve.commands.generate.Reader;
import me.tegeler.evolve.commands.generate.entity.model.Entity;

import java.util.Map;

@Singleton
public class EntityProcessor extends Processor<Map<String, Entity>> {

	@Inject
	public EntityProcessor(
			EntityDeserializer deserializer,
			Reader reader,
			EntityRenderer renderer,
			EntityValidator validator,
			EntityWriter entityWriter
	) {
		super(
				deserializer,
				reader,
				renderer,
				validator,
				entityWriter
		);

	}
}

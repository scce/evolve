package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum InputType {
    @JsonProperty("boolean")
    BOOLEAN,
    @JsonProperty("date")
    DATE,
    @JsonProperty("datetime")
    DATETIME,
    @JsonProperty("email")
    EMAIL,
    @JsonProperty("fixedPoint")
    FIXEDPOINTNUMBER,
    @JsonProperty("int")
    INT,
    @JsonProperty("string")
    STRING
}

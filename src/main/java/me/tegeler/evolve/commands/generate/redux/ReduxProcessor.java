package me.tegeler.evolve.commands.generate.redux;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Processor;
import me.tegeler.evolve.commands.generate.Reader;
import me.tegeler.evolve.commands.generate.redux.model.Redux;

@Singleton
public class ReduxProcessor extends Processor<Redux> {

	@Inject
	public ReduxProcessor(
			ReduxDeserializer deserializer,
			Reader reader,
			ReduxRenderer renderer,
			ReduxValidator validator,
			ReduxWriter writer) {
		super(
				deserializer,
				reader,
				renderer,
				validator,
				writer
		);
	}
}

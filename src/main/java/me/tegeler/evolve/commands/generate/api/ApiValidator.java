package me.tegeler.evolve.commands.generate.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Validator;

@Singleton
public class ApiValidator extends Validator {
	@Inject
	public ApiValidator(ObjectMapper mapper) {
		super(mapper, "api");
	}
}

package me.tegeler.evolve.commands.generate.entity.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EntityType {
    @JsonProperty("boolean")
    BOOLEAN,
    @JsonProperty("number")
    NUMBER,
    @JsonProperty("object")
    OBJECT,
    @JsonProperty("string")
    STRING;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}

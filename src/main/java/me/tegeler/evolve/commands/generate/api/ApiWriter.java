package me.tegeler.evolve.commands.generate.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Writer;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.FileWriter;

import java.io.File;

@Singleton
public class ApiWriter extends Writer {

	@Inject
	public ApiWriter(FileWriter fileWriter) {
		super(fileWriter);
	}

	protected File createTargetFile(Rendered rendered) {
		return actualCreateTargetFile(
				rendered,
				FileNameLibrary.getApiModelFileNameEnding(),
				FileNameLibrary.getApiGeneratedFileNameEnding()
		);
	}
}

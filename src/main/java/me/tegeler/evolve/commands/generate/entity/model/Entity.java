package me.tegeler.evolve.commands.generate.entity.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import me.tegeler.evolve.target.bouncer.json.DecoderField;
import me.tegeler.evolve.target.bouncer.json.JsonBouncerLibrary;
import me.tegeler.evolve.target.typescript.Attribute;
import me.tegeler.evolve.target.typescript.Import;

import java.util.HashSet;
import java.util.stream.Stream;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type",
		visible = true
)
@JsonSubTypes({
		@JsonSubTypes.Type(value = EntityNumber.class, name = "number"),
		@JsonSubTypes.Type(value = EntityString.class, name = "string"),
		@JsonSubTypes.Type(value = EntityObject.class, name = "object"),
		@JsonSubTypes.Type(value = EntityBoolean.class, name = "boolean"),
})
abstract public class Entity {
	EntityType type;
	Boolean array;
	Boolean nullAble;
	Boolean optional;
	String origin;

	public EntityType getType() {
		return type;
	}

	public Boolean getArray() {
		return array;
	}

	public Boolean getNullAble() {
		return nullAble;
	}

	public String getOrigin() {
		return origin;
	}

	public Boolean getOptional() {
		return optional;
	}

	public Import getEntityImports(String name) {
		return null;
	}

	public DecoderField getDecoderField(String entityName, String fieldName) {
		String decoder = getDecoder(entityName);
		if (array) {
			decoder = String.format("%s(%s)", JsonBouncerLibrary.array, decoder);
		}
		if (nullAble) {
			decoder = String.format("%s(%s)", JsonBouncerLibrary.nullable, decoder);
		}
		if (optional) {
			decoder = String.format("%s(%s)", JsonBouncerLibrary.optional, decoder);
		}
		String origin = getOrigin(fieldName);
		return new DecoderField(
				fieldName,
				origin,
				decoder
		);
	}

	public Attribute getAttribute(String name) {
		String type = getRawAttribute();
		type = addArrayBrackets(type);
		type = addNull(type);
		type = addOptional(type);
		return new Attribute(
				name,
				type
		);
	}

	public Stream<String> getJsonBouncerImports(String entityName) {
		HashSet<String> imports = new HashSet<>();
		imports.add(JsonBouncerLibrary.object);
		if (isSelfReference(entityName)) {
			imports.add(JsonBouncerLibrary.lazy);
		}
		if (isBaseType()) {
			imports.add(type.toString());
		}
		if (array) {
			imports.add(JsonBouncerLibrary.array);
		}
		if (nullAble) {
			imports.add(JsonBouncerLibrary.nullable);
		}
		if (optional) {
			imports.add(JsonBouncerLibrary.optional);
		}
		return imports.stream();
	}

	boolean isSelfReference(String entityName) {
		return false;
	}

	String getRawAttribute() {
		return this.type.toString();
	}

	String getDecoder(String entityName) {
		return type + "()";
	}

	private String getOrigin(String fieldName) {
		if (origin == null) {
			return fieldName;
		}
		return origin;
	}

	private String addNull(String type) {
		if (nullAble) {
			type = type + " | null";
		}
		return type;
	}

	private String addOptional(String type) {
		if (optional) {
			type = type + " | undefined";
		}
		return type;
	}

	private String addArrayBrackets(String type) {
		if (array) {
			type = type + "[]";
		}
		return type;
	}

	private boolean isBaseType() {
		return !type.equals(EntityType.OBJECT);
	}

}

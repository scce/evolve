package me.tegeler.evolve.commands.generate.form.template;

import me.tegeler.evolve.commands.generate.form.model.Form;
import me.tegeler.evolve.util.StringHelper;

import java.util.Comparator;
import java.util.stream.Collectors;

class Context {
    private final String nameLowerCase;
    private final String nameUpperCase;
    private final me.tegeler.evolve.commands.generate.form.template.Input[] inputs;

    public Context(
            String name,
            Form form
    ) {
        this.nameLowerCase = StringHelper.firstToLowerCase(name);
        this.nameUpperCase = StringHelper.firstToUpperCase(name);
        this.inputs = createInputs(form);
    }

    private me.tegeler.evolve.commands.generate.form.template.Input[] createInputs(Form formMap) {
        return formMap
                .getInputs()
                .entrySet()
                .stream()
                .map(f -> new me.tegeler.evolve.commands.generate.form.template.Input(f, formMap.getPage()))
                .sorted(Comparator.comparing(me.tegeler.evolve.commands.generate.form.template.Input::getInputNameLowerCase))
                .collect(Collectors.toList())
                .toArray(new me.tegeler.evolve.commands.generate.form.template.Input[]{});
    }


    public String getNameLowerCase() {
        return nameLowerCase;
    }

    public me.tegeler.evolve.commands.generate.form.template.Input[] getInputs() {
        return inputs;
    }

    public String getNameUpperCase() {
        return nameUpperCase;
    }

}

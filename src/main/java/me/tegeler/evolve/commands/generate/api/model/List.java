package me.tegeler.evolve.commands.generate.api.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class List implements MethodInterface {
	private final java.util.List<me.tegeler.evolve.target.http.Parameter> parameters;

	public List(
			@JsonProperty(value = "parameters") Map<String, Parameter> parameters
	) {
		if (parameters != null) {
			this.parameters = parameters
					.entrySet()
					.stream()
					.map(me.tegeler.evolve.target.http.Parameter::new)
					.collect(Collectors.toList());
		} else {
			this.parameters = new ArrayList<>();
		}
	}

	@Override
	public HttpRequestMethod buildHttpRequestMethod(String baseUrl, String name) {
		return HttpRequestMethod.buildList(name, baseUrl, this);
	}

	public java.util.List<me.tegeler.evolve.target.http.Parameter> getParameters() {
		return parameters;
	}

	@Override
	public Stream<String> getEntities() {
		return Stream.empty();
	}
}

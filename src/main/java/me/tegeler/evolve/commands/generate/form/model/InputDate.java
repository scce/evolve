package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.joda.JodaLibrary;
import me.tegeler.evolve.target.typescript.Language;

import java.util.List;

public class InputDate extends Input {
    public InputDate(
            @JsonProperty(value = "default") String defaultValue,
            @JsonProperty(value = "mandatory") boolean mandatory
    ) {
        this.defaultValue = defaultValue;
        this.mandatory = mandatory;
        this.type = InputType.DATE;
        this.inputType = Language.unionWithNull(JodaLibrary.localDateType);
        this.dataType = JodaLibrary.localDateType;
        this.validator = buildValidators();
        this.validationType = buildValidationType();
    }

    @Override
    protected void addMandatoryValidator(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryNullableValidator());
    }

    @Override
    protected void addTypeSpecificValidator(List<String> validators) {
        // not needed
    }

    @Override
    protected void addMandatoryValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryNullableValidatorCall());
    }

    @Override
    protected void addTypeSpecificValidatorCall(List<String> validators) {

    }

    @Override
    protected void addTypeSpecificValidationType(List<String> validationTypes) {
        // not needed
    }

}

package me.tegeler.evolve.commands.generate;

public abstract class Renderer<T> {

	protected abstract Rendered render(Deserialized<T> deserialized);

	protected String getName(Deserialized deserialized, String remove) {
		return deserialized
				.getPath()
				.getFileName()
				.toString()
				.replace(remove, "");
	}


}

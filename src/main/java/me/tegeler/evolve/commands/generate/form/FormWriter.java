package me.tegeler.evolve.commands.generate.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Writer;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.FileWriter;

import java.io.File;

@Singleton
public class FormWriter extends Writer {

    @Inject
    public FormWriter(FileWriter fileWriter) {
        super(fileWriter);
    }

    protected File createTargetFile(Rendered rendered) {
        return actualCreateTargetFile(
                rendered,
                FileNameLibrary.getFormModelFileNameEnding(),
                FileNameLibrary.getFormGeneratedFileNameEnding()
        );
    }
}

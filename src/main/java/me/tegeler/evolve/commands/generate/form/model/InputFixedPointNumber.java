package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.typescript.Language;

import java.util.List;

public class InputFixedPointNumber extends Input {
    public InputFixedPointNumber(
            @JsonProperty(value = "default") String defaultValue,
            @JsonProperty(value = "mandatory") boolean mandatory
    ) {
        this.defaultValue = defaultValue;
        this.mandatory = mandatory;
        this.type = InputType.FIXEDPOINTNUMBER;
        this.inputType = Language.getString();
        this.dataType = Language.getNumber();
        this.validator = buildValidators();
        this.validationType = buildValidationType();
    }

    @Override
    protected void addMandatoryValidator(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryStringValidator());
    }

    @Override
    protected void addTypeSpecificValidator(List<String> validators) {
        //todo make decimals adjustable
        validators.add(InputBouncerLibrary.getStringToFixedPointNumberValidator());
    }

    @Override
    protected void addMandatoryValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getMandatoryStringValidatorCall());
    }

    @Override
    protected void addTypeSpecificValidatorCall(List<String> validators) {
        validators.add(InputBouncerLibrary.getStringToFixedPointNumberValidatorCall());

    }

    @Override
    protected void addTypeSpecificValidationType(List<String> validationTypes) {
        validationTypes.add(InputBouncerLibrary.getInvalidFixedPointNumberType());
    }

}

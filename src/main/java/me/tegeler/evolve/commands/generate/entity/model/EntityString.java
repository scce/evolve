package me.tegeler.evolve.commands.generate.entity.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EntityString extends Entity {
    public EntityString(
            @JsonProperty(value = "array") boolean array,
            @JsonProperty(value = "null") boolean nullAble,
            @JsonProperty(value = "optional") boolean optional,
            @JsonProperty(value = "origin") String origin
    ) {
        this.array = array;
        this.nullAble = nullAble;
        this.origin = origin;
        this.optional = optional;
        this.type = EntityType.STRING;
    }
}

package me.tegeler.evolve.commands.generate.form;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Validator;

@Singleton
public class FormValidator extends Validator {
	@Inject
	public FormValidator(ObjectMapper mapper) {
		super(mapper, "form");
	}
}

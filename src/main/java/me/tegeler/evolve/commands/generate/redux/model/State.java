package me.tegeler.evolve.commands.generate.redux.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import me.tegeler.evolve.target.redux.ActionBuilder;
import me.tegeler.evolve.target.redux.ReducerAction;
import me.tegeler.evolve.target.typescript.Interface;

import java.util.stream.Stream;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type",
		visible = true
)
@JsonSubTypes({
		@JsonSubTypes.Type(value = StateBoolean.class, name = "boolean"),
})
abstract public class State {
	String defaultValue;

	public String getDefault() {
		return defaultValue;
	}

	public abstract Stream<Interface> getActions(String stateName, String storeName, String page);

	public abstract Stream<String> getActionNames(String stateName, String storeName);

	public abstract Stream<ReducerAction> getReducerToggleAction(String stateName);

	public abstract String getType();

	public abstract boolean hasAtLeastOneAction();

	public abstract Stream<ActionBuilder> getActionBuilder(
			String stateName,
			String storeName,
			String page
	);

}

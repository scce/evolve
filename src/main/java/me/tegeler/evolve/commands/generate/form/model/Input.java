package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import me.tegeler.evolve.target.bouncer.input.InputBouncerLibrary;
import me.tegeler.evolve.target.typescript.Attribute;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type",
        visible = true
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = InputBoolean.class, name = "boolean"),
        @JsonSubTypes.Type(value = InputDate.class, name = "date"),
        @JsonSubTypes.Type(value = InputDatetime.class, name = "datetime"),
        @JsonSubTypes.Type(value = InputEmail.class, name = "email"),
        @JsonSubTypes.Type(value = InputFixedPointNumber.class, name = "fixedPoint"),
        @JsonSubTypes.Type(value = InputInt.class, name = "int"),
        @JsonSubTypes.Type(value = InputString.class, name = "string"),
})
public abstract class Input {
    InputType type;
    String defaultValue;
    Boolean mandatory;
    String inputType;
    String dataType;
    String validator;
    String validationType;

    protected abstract void addTypeSpecificValidationType(List<String> validationTypes);

    protected abstract void addMandatoryValidator(List<String> validators);

    protected abstract void addTypeSpecificValidator(List<String> validators);

    protected abstract void addMandatoryValidatorCall(List<String> validators);

    protected abstract void addTypeSpecificValidatorCall(List<String> validators);

    String buildValidators() {
        List<String> validators = new ArrayList<>();
        addTypeSpecificValidatorCall(validators);
        if (this.getMandatory()) {
            addMandatoryValidatorCall(validators);
        }
        if (validators.size() == 0) {
            validators.add(InputBouncerLibrary.getSucceedValidatorCall());
        }
        return composeValidator(validators);
    }

    String buildValidationType() {
        List<String> validationTypes = new ArrayList<>();
        if (this.getMandatory()) {
            addMandatoryValidationType(validationTypes);
        }
        addTypeSpecificValidationType(validationTypes);
        addNullValidationType(validationTypes);
        return String.join(" | ", validationTypes);
    }

    private String composeValidator(List<String> validators) {
        String validatorString = String.join(", ", validators);
        if (validators.size() > 1) {
            return InputBouncerLibrary.getComposeFunctionCall(validatorString);
        }
        return validatorString;
    }

    public Stream<String> getUsedValidators() {
        List<String> validators = new ArrayList<>();
        addTypeSpecificValidator(validators);
        if (this.getMandatory()) {
            addMandatoryValidator(validators);
        }
        if (validators.size() > 1) {
            validators.add(InputBouncerLibrary.composeFunction);
        } else if (validators.size() == 0) {
            validators.add(InputBouncerLibrary.getSucceedValidator());
        }
        return validators.stream();
    }

    public boolean isDate() {
        return type.equals(InputType.DATE) || type.equals(InputType.DATETIME);
    }

    private void addMandatoryValidationType(List<String> validationTypes) {
        validationTypes.add(InputBouncerLibrary.getMandatoryType());
    }

    private void addNullValidationType(List<String> validationTypes) {
        validationTypes.add(InputBouncerLibrary.nullType);
    }

    public InputType getType() {
        return type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public String getInputType() {
        return inputType;
    }

    public String getDataType() {
        return dataType;
    }

    public String getValidator() {
        return validator;
    }

    public String getValidationType() {
        return validationType;
    }

    public Attribute createInputInterface(String name) {
        return new Attribute(name, inputType);
    }

    public Attribute createValidationInterface(String name) {
        return new Attribute(name, validationType);
    }

    public Attribute createDataInterface(String name) {
        String newDataType = dataType;
        if (!mandatory) {
            newDataType = String.format("%s | null", dataType);
        }
        return new Attribute(name, newDataType);
    }

}

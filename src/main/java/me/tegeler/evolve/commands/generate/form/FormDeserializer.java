package me.tegeler.evolve.commands.generate.form;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Deserializer;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.form.model.Form;

import java.io.IOException;

@Singleton
public class FormDeserializer extends Deserializer<Form> {
	@Inject
	public FormDeserializer(ObjectMapper mapper) {
		super(mapper);
	}

	public Deserialized<Form> deserialize(Validated read) throws IOException {
		Form form = mapper.readValue(read.getJson(), Form.class);
		return new Deserialized<>(read.getPath(), form);
	}
}

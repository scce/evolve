package me.tegeler.evolve.commands.generate.redux;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Validator;

@Singleton
public class ReduxValidator extends Validator {
	@Inject
	public ReduxValidator(ObjectMapper mapper) {
		super(mapper, "redux");
	}
}

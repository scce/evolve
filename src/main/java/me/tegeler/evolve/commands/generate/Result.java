package me.tegeler.evolve.commands.generate;

import java.io.File;

public class Result {
    private final String message;

    private Result(String message) {
        this.message = message;
    }

    public static Result createSuccessfulDeleteResult(File file) {
        return new Result(String.format("Delete   %s ✓", file.toPath().getFileName()));
    }

    public static Result createFailedDeleteResult(File file) {
        return new Result(String.format("Delete   %s ✗", file.toPath().getFileName()));
    }

    static Result createExceptionResult(Exception e) {
        return new Result(e.getMessage());
    }

    static Result createSuccessfulRenderedResult(Rendered rendered) {
        return new Result(String.format("Generate %s ✓", rendered.getPath().getFileName()));
    }

    public String getMessage() {
        return message;
    }

}

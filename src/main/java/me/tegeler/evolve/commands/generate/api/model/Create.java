package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.Objects;
import java.util.stream.Stream;

public class Create implements MethodInterface {
    private final String responseEntity;
    private final String requestEntity;

    public Create(
            @JsonProperty(value = "responseEntity", required = true) String responseEntity,
            @JsonProperty(value = "requestEntity", required = true) String requestEntity
    ) {
        this.responseEntity = responseEntity;
        this.requestEntity = requestEntity;
    }

    @Override
    public HttpRequestMethod buildHttpRequestMethod(String baseUrl, String name) {
        return HttpRequestMethod.buildCreate(name, baseUrl, this);
    }

    @Override
    public Stream<String> getEntities() {
        return Stream
                .of(
                        requestEntity,
                        responseEntity
                )
                .filter(Objects::nonNull);
    }

    public String getRequestEntity() {
        return requestEntity;
    }
}

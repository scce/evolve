package me.tegeler.evolve.commands.generate.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Validator;

@Singleton
public class EntityValidator extends Validator {

    @Inject
    public EntityValidator(ObjectMapper mapper) {
        super(mapper, "entity");
    }
}

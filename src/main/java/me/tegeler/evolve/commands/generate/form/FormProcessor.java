package me.tegeler.evolve.commands.generate.form;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Processor;
import me.tegeler.evolve.commands.generate.Reader;
import me.tegeler.evolve.commands.generate.form.model.Form;

@Singleton
public class FormProcessor extends Processor<Form> {
	@Inject
	public FormProcessor(
			FormDeserializer deserializer,
			Reader reader,
			FormRenderer renderer,
			FormValidator validator,
			FormWriter writer
	) {
		super(
				deserializer,
				reader,
				renderer,
				validator,
				writer
		);
	}
}

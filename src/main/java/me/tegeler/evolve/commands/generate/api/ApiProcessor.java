package me.tegeler.evolve.commands.generate.api;

import com.google.inject.Inject;
import me.tegeler.evolve.commands.generate.Processor;
import me.tegeler.evolve.commands.generate.Reader;
import me.tegeler.evolve.commands.generate.api.model.Api;

public class ApiProcessor extends Processor<Api> {

	@Inject
	public ApiProcessor(
			ApiDeserializer deserializer,
			Reader reader,
			ApiRenderer renderer,
			ApiValidator validator,
			ApiWriter writer
	) {
		super(
				deserializer,
				reader,
				renderer,
				validator,
				writer
		);
	}
}

package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Rest {
    private final Create create;
    private final Retrieve retrieve;
    private final Update update;
    private final Delete delete;
    private final List list;

    public Rest(
            @JsonProperty(value = "createReducer") Create create,
            @JsonProperty(value = "retrieve") Retrieve retrieve,
            @JsonProperty(value = "update") Update update,
            @JsonProperty(value = "delete") Delete delete,
            @JsonProperty(value = "list") List list
    ) {
        this.create = create;
        this.retrieve = retrieve;
        this.update = update;
        this.delete = delete;
        this.list = list;
    }

    public Create getCreate() {
        return create;
    }

    public Retrieve getRetrieve() {
        return retrieve;
    }

    public Update getUpdate() {
        return update;
    }

    public Delete getDelete() {
        return delete;
    }

    public List getList() {
        return list;
    }
}

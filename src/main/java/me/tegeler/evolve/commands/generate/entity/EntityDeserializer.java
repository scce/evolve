package me.tegeler.evolve.commands.generate.entity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Deserializer;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.entity.model.Entity;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class EntityDeserializer extends Deserializer<Map<String, Entity>> {

	private final TypeReference<HashMap<String, Entity>> typeRef = new TypeReference<HashMap<String, Entity>>() {
	};

	@Inject
	public EntityDeserializer(ObjectMapper mapper) {
		super(mapper);
	}

	public Deserialized<Map<String, Entity>> deserialize(Validated read) throws IOException {
		Map<String, Entity> map = mapper.readValue(read.getJson(), typeRef);
		return new Deserialized<>(read.getPath(), map);
	}
}

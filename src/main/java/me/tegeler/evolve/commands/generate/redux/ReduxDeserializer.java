package me.tegeler.evolve.commands.generate.redux;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Deserializer;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.redux.model.Redux;

import java.io.IOException;

@Singleton
public class ReduxDeserializer extends Deserializer<Redux> {

	@Inject
	public ReduxDeserializer(ObjectMapper mapper) {
		super(mapper);
	}

	public Deserialized<Redux> deserialize(Validated read) throws IOException {
		Redux redux = mapper.readValue(read.getJson(), Redux.class);
		return new Deserialized<>(read.getPath(), redux);
	}
}

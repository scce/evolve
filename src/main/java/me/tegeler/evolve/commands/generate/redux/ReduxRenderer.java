package me.tegeler.evolve.commands.generate.redux;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Renderer;
import me.tegeler.evolve.commands.generate.redux.model.Redux;
import me.tegeler.evolve.commands.generate.redux.model.State;
import me.tegeler.evolve.target.redux.*;
import me.tegeler.evolve.target.typescript.*;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.StringHelper;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class ReduxRenderer extends Renderer<Redux> {

	private final ActionBuilderGenerator actionBuilderGenerator;
	private final ImportGenerator importGenerator;
	private final InitFunctionGenerator initFunctionGenerator;
	private final InterfaceGenerator interfaceGenerator;
	private final ReducerGenerator reducerGenerator;
	private final UnionTypeGenerator unionTypeGenerator;

	@Inject
	public ReduxRenderer(
			ActionBuilderGenerator actionBuilderGenerator,
			ImportGenerator importGenerator,
			InitFunctionGenerator initFunctionGenerator,
			InterfaceGenerator interfaceGenerator,
			ReducerGenerator reducerGenerator,
			UnionTypeGenerator unionTypeGenerator
	) {
		this.actionBuilderGenerator = actionBuilderGenerator;
		this.importGenerator = importGenerator;
		this.initFunctionGenerator = initFunctionGenerator;
		this.interfaceGenerator = interfaceGenerator;
		this.reducerGenerator = reducerGenerator;
		this.unionTypeGenerator = unionTypeGenerator;
	}

	@Override
	protected Rendered render(Deserialized<Redux> deserialized) {
		String actionBuilder = "";
		String actionUnionType = "";
		String actions = "";
		String imports = "";
		String reducer = "";


		boolean hasAtLeastOneAction = hasAtLeastOneAction(deserialized);
		String storeName = getName(deserialized, FileNameLibrary.getReduxModelFileNameEnding());

		if (hasAtLeastOneAction) {
			actionBuilder = generateActionBuilder(storeName, deserialized);
			actionUnionType = generateActionUnionType(storeName, deserialized);
			actions = generateActions(storeName, deserialized);
			imports = generateImports();
			reducer = generateReducer(storeName, deserialized);
		}

		String state = generateState(storeName, deserialized);
		String initFunction = generateInitFunction(storeName, deserialized);

		return new Rendered(
				deserialized.getPath(),
				StringHelper.concat(
						imports,
						state,
						initFunction,
						actionUnionType,
						actions,
						actionBuilder,
						reducer
				)
		);
	}

	private boolean hasAtLeastOneAction(Deserialized<Redux> deserialized) {
		return createStatesStream(deserialized)
				.anyMatch(stringStateEntry -> stringStateEntry.getValue().hasAtLeastOneAction());
	}

	private String generateActionBuilder(String storeName, Deserialized<Redux> deserialized) {
		return createStatesStream(deserialized)
				.flatMap(stringInputEntry -> createActionBuilder(storeName, deserialized.getPayload().getPage(), stringInputEntry))
				.map(actionBuilderGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private String generateImports() {
		return importGenerator.generate(Import.createImportForReducer()).getResult();
	}

	private String generateReducer(String storeName, Deserialized<Redux> deserialized) {
		List<ReducerAction> toggleActionList = createStatesStream(deserialized)
				.flatMap(ReduxRenderer::createReducerToggleAction)
				.collect(Collectors.toList());
		return reducerGenerator.generate(Reducer.createReducer(storeName, toggleActionList)).getResult();
	}


	private String generateState(String storeName, Deserialized<Redux> deserialized) {
		List<Attribute> attributes = createStatesStream(deserialized)
				.map(Attribute::createReduxStateAttribute)
				.collect(Collectors.toList());
		return interfaceGenerator.generate(Interface.createReduxStateInterface(storeName, attributes)).getResult();
	}

	private String generateInitFunction(String storeName, Deserialized<Redux> deserialized) {
		List<VariableValue> variableValueList = createStatesStream(deserialized)
				.map(VariableValue::createFromReduxStateEntry)
				.collect(Collectors.toList());
		return initFunctionGenerator
				.generate(
						new InitFunction(
								storeName,
								variableValueList
						)
				).getResult();
	}

	private String generateActionUnionType(String storeName, Deserialized<Redux> deserialized) {
		List<String> subTypes = createStatesStream(deserialized)
				.flatMap(state -> state.getValue().getActionNames(state.getKey(), storeName))
				.collect(Collectors.toList());
		return unionTypeGenerator.generate(
				UnionType.createUnionTypeForRedux(
						storeName,
						subTypes
				)
		).getResult();
	}

	private String generateActions(String storeName, Deserialized<Redux> deserialized) {
		return createStatesStream(deserialized)
				.flatMap(stringInputEntry -> createActions(storeName, deserialized.getPayload().getPage(), stringInputEntry))
				.map(interfaceGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private Stream<Map.Entry<String, State>> createStatesStream(Deserialized<Redux> deserialized) {
		return deserialized
				.getPayload()
				.getStates()
				.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getKey));
	}

	private static Stream<ReducerAction> createReducerToggleAction(Map.Entry<String, State> entry) {
		return entry.getValue().getReducerToggleAction(entry.getKey());
	}

	private static Stream<ActionBuilder> createActionBuilder(
			String storeName,
			String pageName,
			Map.Entry<String, State> entry
	) {
		return entry
				.getValue()
				.getActionBuilder(
						entry.getKey(),
						storeName,
						pageName
				);
	}

	private Stream<Interface> createActions(
			String storeName,
			String pageName,
			Map.Entry<String, State> entry
	) {
		return entry
				.getValue()
				.getActions(
						entry.getKey(),
						storeName,
						pageName
				);
	}

}

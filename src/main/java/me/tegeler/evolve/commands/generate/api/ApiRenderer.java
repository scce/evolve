package me.tegeler.evolve.commands.generate.api;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Renderer;
import me.tegeler.evolve.commands.generate.api.model.Api;
import me.tegeler.evolve.commands.generate.api.model.MethodInterface;
import me.tegeler.evolve.target.http.HttpRequestMethod;
import me.tegeler.evolve.target.http.HttpRequestMethodGenerator;
import me.tegeler.evolve.target.typescript.Import;
import me.tegeler.evolve.target.typescript.ImportGenerator;
import me.tegeler.evolve.target.typescript.PartialGeneratorResult;
import me.tegeler.evolve.util.StringHelper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton
public class ApiRenderer extends Renderer<Api> {
	private final HttpRequestMethodGenerator generator;
	private final ImportGenerator importGenerator;

	@Inject
	public ApiRenderer(
			HttpRequestMethodGenerator generator,
			ImportGenerator importGenerator
	) {
		this.generator = generator;
		this.importGenerator = importGenerator;
	}

	@Override
	protected Rendered render(Deserialized<Api> deserialized) {
		String imports = renderImports(deserialized);
		String methods = renderMethods(deserialized);
		String api = StringHelper.concat(imports, methods);
		return new Rendered(deserialized.getPath(), api);
	}

	private String renderImports(Deserialized<Api> deserialized) {
		return createImports(deserialized)
				.stream()
				.map(importGenerator::generate)
				.filter(PartialGeneratorResult::isSuccess)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private List<Import> createImports(Deserialized<Api> deserialized) {
		return extractEntitiesImports(deserialized).collect(Collectors.toList());
	}

	private Stream<Import> extractEntitiesImports(Deserialized<Api> deserialized) {
		return deserialized.getPayload()
				.getRestAndMethods()
				.flatMap(MethodInterface::getEntities)
				.distinct()
				.map(Import::createImportFromEntity);
	}


	private String renderMethods(Deserialized<Api> deserialized) {
		return deserialized
				.getPayload()
				.getRestAndMethods()
				.map(methodInterface -> getHttpRequestMethod(deserialized, methodInterface))
				.map(generator::generate)
				//.filter(PartialGeneratorResult::isSuccess)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat)
				.orElse("");
	}

	private HttpRequestMethod getHttpRequestMethod(Deserialized<Api> deserialized, MethodInterface methodInterface) {
		return methodInterface.buildHttpRequestMethod(
				deserialized.getPayload().getPath(),
				getName(deserialized, ".api.json")
		);
	}

}

package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ParameterType {
	@JsonProperty("string")
	STRING,
	@JsonProperty("number")
	NUMBER;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}

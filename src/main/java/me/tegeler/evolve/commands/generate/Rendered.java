package me.tegeler.evolve.commands.generate;

import java.nio.file.Path;

public class Rendered {

    private final Path path;
    private final String rendered;

    public Rendered(Path path, String rendered) {
        this.path = path;
        this.rendered = rendered;
    }

    public Path getPath() {
        return path;
    }

    public String getRendered() {
        return rendered;
    }
}

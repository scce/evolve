package me.tegeler.evolve.commands.generate.entity.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.typescript.Import;
import me.tegeler.evolve.util.StringHelper;

public class EntityObject extends Entity {
	private final String entity;

	public EntityObject(
			@JsonProperty(value = "array") boolean array,
			@JsonProperty(value = "null") boolean nullAble,
			@JsonProperty(value = "optional") boolean optional,
			@JsonProperty(value = "origin") String origin,
			@JsonProperty(required = true, value = "entity") String entity
	) {
		this.array = array;
		this.nullAble = nullAble;
		this.origin = origin;
		this.optional = optional;
		this.type = EntityType.OBJECT;
		this.entity = entity;
	}

	@Override
	public String getDecoder(String entityName) {
		String decoder = StringHelper.firstToLowerCase(entity) + "Decoder";
		if(isSelfReference(entityName)) {
			return String.format("lazy(%s)", decoder);
		}
		return decoder + "()";
	}

	@Override
	public Import getEntityImports(String entityName) {
		if (isSelfReference(entityName)) {
			return null;
		}
		return Import.createForEntity(entity);
	}

	@Override
	boolean isSelfReference(String entityName) {
		return entity.equalsIgnoreCase(entityName);
	}

	@Override
	protected String getRawAttribute() {
		return StringHelper.firstToUpperCase(entity);
	}

}

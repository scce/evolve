package me.tegeler.evolve.commands.generate.entity;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Rendered;
import me.tegeler.evolve.commands.generate.Renderer;
import me.tegeler.evolve.commands.generate.entity.model.Entity;
import me.tegeler.evolve.target.bouncer.json.Decoder;
import me.tegeler.evolve.target.bouncer.json.DecoderField;
import me.tegeler.evolve.target.bouncer.json.DecoderGenerator;
import me.tegeler.evolve.target.typescript.*;
import me.tegeler.evolve.util.FileNameLibrary;
import me.tegeler.evolve.util.StringHelper;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Singleton
public class EntityRenderer extends Renderer<Map<String, Entity>> {
	private final ImportGenerator importGenerator;
	private final InterfaceGenerator interfaceGenerator;
	private final DecoderGenerator decoderGenerator;

	@Inject
	public EntityRenderer(
			ImportGenerator importGenerator,
			InterfaceGenerator interfaceGenerator,
			DecoderGenerator decoderGenerator
	) {
		this.importGenerator = importGenerator;
		this.interfaceGenerator = interfaceGenerator;
		this.decoderGenerator = decoderGenerator;
	}

	public Rendered render(Deserialized<Map<String, Entity>> deserialized) {
		String entityName = getName(deserialized, FileNameLibrary.getEntityModelFileNameEnding());
		String bouncerImports = renderJsonBouncerImports(entityName, deserialized);
		String entityImports = renderEntityImports(entityName, deserialized);
		String interfaceString = renderInterface(entityName, deserialized);
		String decoder = renderDecoder(entityName, deserialized);
		String api = StringHelper.concat(bouncerImports, entityImports, interfaceString, decoder);
		return new Rendered(deserialized.getPath(), api);
	}

	private String renderEntityImports(String entityName, Deserialized<Map<String, Entity>> deserialized) {
		return deserialized
				.getPayload()
				.entrySet()
				.stream()
				.map(stringEntityEntry -> stringEntityEntry.getValue().getEntityImports(entityName))
				.filter(Objects::nonNull)
				.collect(Collectors.toSet())
				.stream()
				.map(importGenerator::generate)
				.map(PartialGeneratorResult::getResult)
				.reduce(StringHelper::concat).orElse("");
	}

	private String renderJsonBouncerImports(String entityName, Deserialized<Map<String, Entity>> deserialized) {
		return importGenerator.generate(
				Import.createImportForJsonBouncer(
						deserialized
								.getPayload()
								.values()
								.stream()
								.flatMap((Entity entry) -> entry.getJsonBouncerImports(entityName))
								.collect(Collectors.toSet())
				)
		).getResult();
	}

	private String renderInterface(String entityName, Deserialized<Map<String, Entity>> deserialized) {
		return interfaceGenerator.generate(
				new Interface(
						entityName,
						deserialized
								.getPayload()
								.entrySet()
								.stream()
								.map(EntityRenderer::getAttribute)
								.collect(Collectors.toList()),
						true
				)
		).getResult();
	}

	private static Attribute getAttribute(Map.Entry<String, Entity> entityEntry) {
		return entityEntry.getValue().getAttribute(entityEntry.getKey());
	}

	private String renderDecoder(String entityName, Deserialized<Map<String, Entity>> deserialized) {
		return decoderGenerator.generate(
				new Decoder(
						entityName,
						deserialized
								.getPayload()
								.entrySet()
								.stream()
								.map((Map.Entry<String, Entity> entry) -> getDecoderField(entityName, entry))
								.collect(Collectors.toList())
				)
		).getResult();
	}

	private static DecoderField getDecoderField(String entityName, Map.Entry<String, Entity> stringEntityEntry) {
		return stringEntityEntry.getValue().getDecoderField(entityName, stringEntityEntry.getKey());
	}

}

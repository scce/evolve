package me.tegeler.evolve.commands.generate;

import java.nio.file.Path;

public class Validated {

    private final Path path;
    private final String json;

    public Validated(Path path, String json) {
        this.path = path;
        this.json = json;
    }

    public Path getPath() {
        return path;
    }

    public String getJson() {
        return json;
    }
}


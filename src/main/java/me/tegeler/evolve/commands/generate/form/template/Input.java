package me.tegeler.evolve.commands.generate.form.template;

import me.tegeler.evolve.util.StringHelper;

import java.util.Map;

public class Input {

    private final String inputNameLowerCase;
    private final String inputNameUpperCase;
    private final String inputType;
    private final String validationType;
    private final String validator;
    private final String dataType;
    private final String page;

    public Input(Map.Entry<String, me.tegeler.evolve.commands.generate.form.model.Input> entry, String page) {
        this.page = StringHelper.firstToUpperCase(page);
        String name = entry.getKey();
        this.inputNameLowerCase = StringHelper.firstToLowerCase(name);
        this.inputNameUpperCase = StringHelper.firstToUpperCase(name);
        me.tegeler.evolve.commands.generate.form.model.Input input = entry.getValue();
        this.validationType = input.getValidationType();
        this.validator = input.getValidator();
        this.inputType = input.getInputType();
        this.dataType = input.getDataType();
    }

    public String getValidator() {
        return validator;
    }

    String getInputNameLowerCase() {
        return inputNameLowerCase;
    }

    String getInputNameUpperCase() {
        return inputNameUpperCase;
    }

    public String getInputType() {
        return inputType;
    }

    String getValidationType() {
        return validationType;
    }

    String getDataType() {
        return dataType;
    }

    public String getPage() {
        return page;
    }

}

package me.tegeler.evolve.commands.generate.redux.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.redux.ActionBuilder;
import me.tegeler.evolve.target.redux.NameCreator;
import me.tegeler.evolve.target.redux.ReducerAction;
import me.tegeler.evolve.target.typescript.Interface;
import me.tegeler.evolve.target.typescript.Language;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class StateBoolean extends State {
	private final boolean hasToggle;
	private final boolean hasDisable;

	public StateBoolean(
			@JsonProperty(value = "toggle") boolean hasToggle,
			@JsonProperty(value = "disable") boolean hasDisable,
			@JsonProperty(value = "default") String defaultValue
	) {
		this.defaultValue = defaultValue;
		this.hasDisable = hasDisable;
		this.hasToggle = hasToggle;
	}


	public Stream<Interface> getActions(String stateName, String storeName, String page) {
		List<Interface> actionList = new ArrayList<>();
		if (hasDisable) {
			actionList.add(
					Interface.createReduxDisableActionInterface(
							stateName,
							storeName,
							page
					)
			);
		}
		if (hasToggle) {
			actionList.add(
					Interface.createReduxToggleActionInterface(
							stateName,
							storeName,
							page
					)
			);
		}
		return actionList.stream();
	}

	public Stream<ReducerAction> getReducerToggleAction(String stateName) {
		List<ReducerAction> reducerActionList = new ArrayList<>();
		if (hasDisable) {
			reducerActionList.add(ReducerAction.createDisableAction(stateName));
		}
		if (hasToggle) {
			reducerActionList.add(ReducerAction.createToggleAction(stateName));
		}
		return reducerActionList.stream();
	}

	public Stream<String> getActionNames(String stateName, String storeName) {
		List<String> actionNameList = new ArrayList<>();
		if (hasDisable) {
			actionNameList.add(NameCreator.createNameReduxDisableAction(stateName, storeName));
		}
		if (hasToggle) {
			actionNameList.add(NameCreator.createNameReduxToggleAction(stateName, storeName));
		}
		return actionNameList.stream();
	}


	public String getType() {
		return Language.getBoolean();
	}

	@Override
	public boolean hasAtLeastOneAction() {
		return hasToggle || hasDisable;
	}

	@Override
	public Stream<ActionBuilder> getActionBuilder(
			String stateName,
			String storeName,
			String page
	) {
		List<ActionBuilder> actionBuilderList = new ArrayList<>();
		if (hasDisable) {
			actionBuilderList.add(ActionBuilder.createDisableAction(stateName, storeName, page));
		}
		if (hasToggle) {
			actionBuilderList.add(ActionBuilder.createToggleAction(stateName, storeName, page));
		}
		return actionBuilderList.stream();
	}
}

package me.tegeler.evolve.commands.generate.form.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Form {
    private final String page;
    private final Map<String, Input> inputs;

    public Form(
            @JsonProperty(value = "page") String page,
            @JsonProperty(value = "inputs") Map<String, Input> inputs
    ) {
        this.page = page;
        this.inputs = inputs;
    }

    public String getPage() {
        return page;
    }

    public Map<String, Input> getInputs() {
        return inputs;
    }
}

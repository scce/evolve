package me.tegeler.evolve.commands.generate.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import me.tegeler.evolve.commands.generate.Deserialized;
import me.tegeler.evolve.commands.generate.Deserializer;
import me.tegeler.evolve.commands.generate.Validated;
import me.tegeler.evolve.commands.generate.api.model.Api;

import java.io.IOException;

@Singleton
public class ApiDeserializer extends Deserializer<Api> {

    private final TypeReference<Api> typeRef = new TypeReference<Api>() {
    };

    @Inject
    public ApiDeserializer(ObjectMapper mapper) {
        super(mapper);
    }

    public Deserialized<Api> deserialize(Validated read) throws IOException {
        Api api = mapper.readValue(read.getJson(), typeRef);
        return new Deserialized<>(read.getPath(), api);
    }
}

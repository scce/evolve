package me.tegeler.evolve.commands.generate.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.tegeler.evolve.target.http.Header;
import me.tegeler.evolve.target.http.HttpRequestMethod;

import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Method implements MethodInterface {
	private final String name;
	private final MethodEnum method;
	private final String path;
	private final String responseEntity;
	private final String requestEntity;
	private final List<me.tegeler.evolve.target.http.Parameter> parameters;
	private final List<Header> headers;

	public Method(
			@JsonProperty(value = "name", required = true) String name,
			@JsonProperty(value = "method", required = true) MethodEnum method,
			@JsonProperty(value = "path", required = true) String path,
			@JsonProperty(value = "responseEntity") String responseEntity,
			@JsonProperty(value = "requestEntity") String requestEntity,
			@JsonProperty(value = "parameters") Map<String, Parameter> parameters,
			@JsonProperty(value = "headers") List<Header> headers
	) {
		this.name = name;
		this.method = method;
		this.path = path;
		this.responseEntity = responseEntity;
		this.requestEntity = requestEntity;
		this.headers = headers;
		if (parameters != null) {
			this.parameters = parameters
					.entrySet()
					.stream()
					.map(me.tegeler.evolve.target.http.Parameter::new)
					.collect(Collectors.toList());
		} else {
			this.parameters = new ArrayList<>();
		}
	}

	public String getName() {
		return name;
	}

	public MethodEnum getMethod() {
		return method;
	}

	public String getPath() {
		return path;
	}

	public String getRequestEntity() {
		return requestEntity;
	}

	public List<Header> getHeaders() {
		List<Header> headers = getListOrEmptyList(this.headers);
		if (this.responseEntity != null) {
			headers.add(Header.getAcceptHeader());
		}
		if (this.requestEntity != null) {
			headers.add(Header.getContentTypeHeader());
		}
		return headers;
	}

	public List<me.tegeler.evolve.target.http.Parameter> getParameters() {
		List<me.tegeler.evolve.target.http.Parameter> parameters = getListOrEmptyList(this.parameters);
		if (this.requestEntity != null) {
			parameters.add(me.tegeler.evolve.target.http.Parameter.getRequestEntity(this.requestEntity));
		}
		return parameters;
	}

	private static <T> List<T> getListOrEmptyList(List<T> list) {
		if (list == null) {
			return new ArrayList<>();
		}
		return list;
	}

	@Override
	public HttpRequestMethod buildHttpRequestMethod(String baseUrl, String name) {
		return HttpRequestMethod.build(baseUrl, this);
	}

	@Override
	public Stream<String> getEntities() {
		return Stream
				.of(
						requestEntity,
						responseEntity
				)
				.filter(Objects::nonNull);
	}
}
